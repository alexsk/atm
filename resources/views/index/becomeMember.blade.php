@extends('layouts.landing')
@section('title', 'become-member ')
@section('modal')
@include('index._modal')
@endsection
@section('content')

        <!-- CRITICAL CSS -->
<style>
    .payment-plans-container .option-trial {
        margin-bottom: 30px;
    }

    @media (min-width: 1200px) {
        .col-lg-offset-3 {
            margin-left: 25%;
        }
    }

</style>

<div class="become-member-container">
    <div class="section">
        <div class="row products-row " style="margin-bottom: 0px;">
            <div class="products-column">
                <h3>CHOOSE YOUR PLAN TO CONTINUE</h3>
            </div>
        </div>
    </div>
    <div class="payment-plans-container">
        <div class="section">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="row option-container">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="option-one-monthly option-block-container">
                                <div class="option-block">
                                    <h2>FREE TRIAL</h2>

                                    <div class="payment-optional">
                                        <h3><strong>A$0</strong> / 15 days</h3>

                                        <p class="optional-time">Limited Time Offer

                                        <div><a href="">
                                                <form action="/thanks" method="GET" target="_top">
                                                    <input type="hidden" name="tx" value="trial0002016">
                                                    <input class="button" type="submit" name="submit" value="Join Now"
                                                           alt="Australasian Trading Management - Trial Subsription"
                                                           onClick="ga('send', 'event', 'join_one_month', 'Join');">
                                                </form>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="option-six-monthly option-block-container">
                                <div class="option-block">
                                    <h2>6 months subscription</h2>

                                    <div class="payment-optional">
                                        <h3><strong>A$100</strong> / month</h3>

                                        <p class="optional-time"><br><br>
                                            *equates to $600 over 6 months period</p>

                                        <div><a href="">
                                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                                      target="_top">
                                                    <input type="hidden" name="cmd" value="_s-xclick">
                                                    <input type="hidden" name="hosted_button_id" value="RHCMNYGSULED6">
                                                    <input class="button" type="submit" name="submit" value="Join Now"
                                                           alt="PayPal - The safer, easier way to pay online!"
                                                           onClick="ga('send', 'event', 'join_six_month', 'Join');">
                                                </form>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="option-twelve-monthly option-block-container">
                                <div class="option-block">
                                    <h2>12 months subscription</h2>

                                    <div class="payment-optional">
                                        <h3><strong>A$83.33</strong> / month</h3>

                                        <p class="optional-time">Save 20% p.a<br><br>
                                            *equates to $995 over 12 months period</p>

                                        <div>
                                            <a href="" target="_blank">
                                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                                      target="_top">
                                                    <input type="hidden" name="cmd" value="_s-xclick">
                                                    <input type="hidden" name="hosted_button_id" value="Y6T7VA5LH3GWE">
                                                    <input class="button" type="submit" name="submit" value="Join Now"
                                                           alt="PayPal - The safer, easier way to pay online!"
                                                           onClick="ga('send', 'event', 'join_year', 'Join');">
                                                </form>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @include('partials.becomeMember')
</div>


@endsection