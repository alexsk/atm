@extends('layouts.landing')
@section('title', 'Thanks for your interest ')
@section('content')

    <div class="container-account-locked">
        <img src="/img/chart_up.jpg" width="100%" height="100%" alt="chart-up"/>
        <h2>Thanks for your interest! We'll contact you soon.</h2>
        <p class="account-locked-description">
            Please click the button and we'll redirect you to the homepage.
        </p>

        <a href="https://atmstrategy.com.au" class="button " title="Go to Homepage">Go to Homepage</a>

    </div>

@endsection