@if($ytdNotes)
    <div class="container_who_we_are">
        <section class="section">
            <h3 class="section-title">Some of Our Recent Picks</h3>

            <div class="row">
                <div class="col-sm-10 col-sm-push-1">
                    <div class="recent-notes">
                        @foreach($ytdNotes as $note)
                            <div class="note-item">
                                {{--<div class="project-image wow fadeInUp-one">--}}
                                <div class="project-image">
                                    <a href="/notes/{{$note->slug}}">
                                        <img src="/uploads-min{{Croppa::url($note->image, 475, 475, ['resize'])}}"
                                             style="margin-top: -50px;" alt="{{$note->title}}"/>
                                    </a>
                                </div>

                                <div class="description_main_image">
                                    <a href="/notes/{{$note->slug}}">
                                        <h4>{{$note->title}}</h4>
                                    </a>

                                    <div class="project-info">
                                        <i class="fontello-icon icon-up-bold"></i><span>{{$note->ytd}}% <small>Last 12
                                                Months
                                            </small></span>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </section>
    </div>
@endif
