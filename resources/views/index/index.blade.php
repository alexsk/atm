@extends('layouts.landing')
@section('title', 'Main page ')
@section('scripts')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>

    <script>
        $(document).ready(function () {
            var $registerModal = $('#registerModal');

            $registerModal.insertBefore($('.wrapper'));
            var interval = setTimeout(function () {
                if (!$('body').hasClass('modal-open')) {
                    $registerModal.modal('show');
                } else {
                    window.clearTimeout(interval);
                    $('#atm-modal').on('hidden.bs.modal', function () {
                        interval = setTimeout(function () {
                            $registerModal.modal('show');
                        }, 30000)
                    });

                }
            }, 30000);

            $('.recent-notes').slick({
                slidesToShow: 3,
                dots: true,
                nav: true,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });
    </script>


    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>

    <script>
      var dataAUPortfolio = $('#auPortfolioChart').data('csv');
      var dataNZPortfolio = $('#nzPortfolioChart').data('csv');

      var data = {};
      data['AUPortfolio'] = {};
      data['NZPortfolio'] = {};

      var au_x1 = dataAUPortfolio[0][1];
      var au_x2 = dataAUPortfolio[dataAUPortfolio.length - 1][1];
      data['AUPortfolio']['portfolio'] = Math.round((au_x2 * 100 / au_x1 - 100) * 10) / 10;

      var au_y1 = dataAUPortfolio[0][2];
      var au_y2 = dataAUPortfolio[dataAUPortfolio.length - 1][2];
      data['AUPortfolio']['benchmark'] = Math.round((au_y2 * 100 / au_y1 - 100) * 10) / 10;

      var nz_x1 = dataNZPortfolio[0][1];
      var nz_x2 = dataNZPortfolio[dataNZPortfolio.length - 1][1];
      data['NZPortfolio']['portfolio'] = Math.round((nz_x2 * 100 / nz_x1 - 100) * 10) / 10;

      var nz_y1 = dataNZPortfolio[0][2];
      var nz_y2 = dataNZPortfolio[dataNZPortfolio.length - 1][2];
      data['NZPortfolio']['benchmark'] = Math.round((nz_y2 * 100 / nz_y1 - 100) * 10) / 10;

      // Create the chart
      Highcharts.chart('container-column', {
        chart: {
          type: 'column',
          backgroundColor: '#d6e5f8'
        },
        title: {
          text: 'Portfolio Performance'
        },
        subtitle: {
          text: 'Returns Since September 2015'
        },
        xAxis: {
          categories: [
            'AU Portfolio',
            'ASX 200',
            'NZ Portfolio',
            'NZX 50'
          ]
        },
        yAxis: {
          title: {
            enabled: false
          },
          labels: {
            enabled: false
          }
        },
        plotOptions: {
          column: {
            pointPadding: 0.1,
            borderWidth: 0,
            colorByPoint: true
          }
        },
        colors: ['#7cb5ec', '#434348'],
        legend: {
          enabled: false
        },
        tooltip: {
          pointFormat: 'Value: <b>{point.y} %</b>'
        },
        colorByPoint: true,
        series: [{
          name: 'Value',
          data: [
            ['AU Portfolio', data['AUPortfolio']['portfolio']],
            ['ASX 200', data['AUPortfolio']['benchmark']],
            ['NZ Portfolio', data['NZPortfolio']['portfolio']],
            ['NZX 50', data['NZPortfolio']['benchmark']]
          ],
          dataLabels: {
            enabled: true,
            color: 'black',
            align: 'center',
            format: '{y} %',
            style: {
              fontSize: '13px',
              "fontFamily": "Open Sans"
            }
          }
        }]
      });
    </script>

@endsection

@section('modal')
    @include('index._modal')
@endsection

@section('style')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"/>

    <!-- CRITICAL CSS -->
    <style>
        .video-container {
            top: 50%;
            width: 100%;
            max-width: 650px;

            position: absolute;
            right: 20px;
            z-index: 1000;
            margin-top: -170px;
        }

        .video {
            position: relative;
            padding-bottom: 56.25%;
            padding-top: 5px;
            height: 0;
            overflow: hidden;
        }

        .video-container iframe, .video-container object, .video-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .slider-container {
            position: relative;
        }

        @media screen and (max-width: 991px) {
            .video-container {
                display: none;
            }
        }

        .banner {
            padding-top: 60px;
            padding-bottom: 25px;
            background: url(../../img/banner.png) 0 0px no-repeat !important;

            background-size: cover !important;
        }

        .banner-title {
            color: white;
            line-height: 1.2;
        }

        .banner .button-container {
            position: relative;
            top: 125px;
            width: 100%;
        }

        .banner .row {
            height: 100%;
        }

        .banner .banner-column {
            position: relative;
            height: 100%;
        }

        .banner ul {
            text-align: left;
        }

        .banner .banner-description {
            display: inline-block;
            position: relative;
            top: 310px;
            left: 15px;
            color: white;
            font-size: 2.2em;
            font-weight: 600;
            line-height: 100%;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .banner li {
            padding-left: 15px;
            padding-right: 15px;
            border-left: 3px solid white;
            float: left;
        }

        .banner li:first-child {
            padding-left: 0;
            border-left: none;
        }

        @media screen and (min-width: 992px) {
            .banner .banner-column:first-child {
                margin-left: -30px;
            }
        }

        .banner h2.banner-title {
            text-align: center;
            margin-top: 20vh;
            font-size: 2.3em;
        }

        .banner h2.banner-title small {
            font-size: 0.75em;
        }

        .banner .button.biggest {
            text-align: center;
            margin-top: 30px;
            margin-top: 50px !important;
        }

        .banner {
            height: 75vh;
            min-height: 500px;
        }

        .text-center {
            text-align: center;
        }

        .services-list .services-item {
            text-align: center;
        }

        .services-list .services-item img {
            margin-bottom: 20px;
        }

        .services-list .services-item h3 {
            margin-bottom: 15px;
            font-weight: 300;
            color: #fff;
            font-size: 1.8em;
        }

        .services-list .services-item p {
            line-height: 22px;
            color: #8cd9ff;
            font-size: 16px;
        }

        .section-title {
            text-transform: capitalize;
            font-size: 2.1em;
            font-weight: 400;
            line-height: 1;
            margin-bottom: 25px;
            font-weight: 400;
            text-align: center;
            text-transform: capitalize;
        }

        .section-title.white {
            color: #fff;
        }

        .testimonial {
            margin-top: 0;
        }

        .container_who_we_are {
            background: #fff;
        }

        .project-image img {
            width: 100%;
        }

        .note-item {
            padding-left: 20px;
            padding-right: 20px;
            overflow: hidden;
        }

        .description_main_image {
            background: #fff;
            padding: 20px;
        }

        .project-info span {
            color: #0190d4;
        }

        .description_main_image small {
            color: #2a3b45;
        }

        .description_main_image h4 {
            color: #2a3b45;
            text-transform: none;
        }

        .note-item .project-image {
            max-height: 350px;
        }

        .slick-next:before, .slick-prev:before {
            font-size: 35px;
            color: #0095d7;
        }

        .slick-next, .slick-prev {
            width: 35px;
            height: 35px;
            top: 40%;
        }

        .slick-prev {
            left: -35px;
        }

        .slick-next {
            right: -35px;
        }

        .container_freetrial h3 {
            font-size: 1.8em;
            color: #23323a;
            line-height: 120%;
        }

        @media screen and (max-width: 992px) {
            .services-item {
                margin-bottom: 20px;
            }
        }

        @media screen and (max-width: 768px) {
            .services-list {
                margin-top: 0;
            }

            .slick-next, .slick-prev {
                display: none !important;
            }
        }

        .banner {
            height: auto !important;
        }

        .banner h1 {
            padding-top: 80px;
            font-size: 55px;
            font-weight: bold;
            color: #fff;
            line-height: 55px;
            margin-bottom: 30px;
        }

        .banner h2 {
            font-size: 45px;
            font-weight: bold;
            color: #fff;
            line-height: 45px;
        }

        .banner ul {
            margin-top: 36px;
        }

        .banner li {
            display: block;
            font-size: 24px;
            color: #fff;
            width: 100%;
            border: none !important;
            padding-left: 50px !important;
            margin-bottom: 30px;
        }

        .banner li img {
            margin-top: -4px;
            margin-right: 10px;
        }

        .trial-block {
            padding: 40px;
            padding-top: 140px;
            max-width: 500px;
        }

        .trial-header {
            background-color: #afcbd5;
            width: 100%;
            min-height: 60px;
            text-align: center;
            padding-top: 12px;
            padding-bottom: 15px;
            font-size: 32px;
            color: #072f4c;
            font-weight: bold;
            font-family: "Open Sans", sans-serif;
            line-height: 36px;
        }

        .trial-body {
            background: #0093cf;
            min-height: 155px;
            color: #fff;
            font-size: 18px;
            font-weight: bold;
            text-align: center;
            padding-top: 20px;
            padding-left: 30px;
            padding-right: 30px;
        }

        .trial-body a {
            background-color: #072f4c !important;
            margin-top: 20px;
        }

        .container_services_list {
            padding: 60px 0;
            background: url(../../img/banner2.png) no-repeat !important;

            background-size: cover !important;
            background-size: cover;
        }
        .chartSlider {

        }
        .section-2 {
            padding-top: 75px;
        }
        .section-2 h1 {
            font-size: 34px;
            text-align: center;
            margin-bottom: 35px;
        }

        .post_thumbnail img {
            border: 1px solid #aeaeae !important;
        }
        .post_content .post_content_text {
             max-height: 70px;
        }
    </style>
@endsection

@section('content')

    <div class="page-content clearfix">

        <main class="main">
            <section class="banner">
                <section class="section">

                <div class="row">
                    <div class="col-md-6">
                        <h1>
                            ATM Research
                        </h1>
                        <h2>
                            Stock Markets, <span style="color: #34b0ed">Simplified</span>.
                        </h2>
                        <ul>
                            <li><img src="/img/checkbox.png" alt=""> Independent Research</li>
                            <li><img src="/img/checkbox.png" alt=""> Jargon Free Analysis</li>
                            <li><img src="/img/checkbox.png" alt=""> Transparent Performance</li>
                        </ul>
                    </div>
                    <div class="col-md-6" align="center">
                        <div class="trial-block">
                            <div class="trial-header">
                                <img src="/img/arrow.png" alt=""> 15 DAY FREE TRIAL
                            </div>
                            <div class="trial-body">
                                <div>
                                    Access our expert stock market research FREE of charge with no obligation
                                </div>

                                <a href="/auth/register" class="button" title="Sign up" style="font-size: 1.2em">
                                    SIGN UP FOR FREE TODAY
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                </section>
            </section>




            <div class="clearfix container_recent_posts">

                @include('index._recent-posts')

            </div>

            <div class="container_services_list clearfix">
                <section class="section">

                    {{--<h3 class="section-title white">The ATM Difference</h3>--}}

                    <div class="row clearfix services-list">
                        <div class="col-md-1 col-sm-6"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="services-item wow fadeInUp-two" data-wow-offset="50">
                                <img src="img/loupe.png" alt="loupe">

                                <h3>Independent Research </h3>

                                <p>ATM is an independent research house, with a team of experienced analysts.</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="services-item wow fadeInUp-two" data-wow-offset="50" data-wow-delay="0.45s">
                                <img src="img/airplane.png" alt="airplane">

                                <h3>Simplistic</h3>

                                <p>We make our research easy to understand and concise - jargon free.</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6" data-wow-offset="50" data-wow-delay="0.15s">
                            <div class="services-item wow fadeInUp-two" data-wow-offset="50" data-wow-delay="0.15s">
                                <img src="img/compass.png" alt="compas">

                                <h3>Performance </h3>

                                <p>We run transparent model portfolios with a proven track record.</p>
                            </div>
                        </div>

                    </div>

                </section>
            </div>



            <div class="container_freetrial" style="background-color: #ddf0f6">
                <section class="section">
                    <div class="row row-inline align-middle">
                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <h3>
                                <span>Free Daily Market Insights:</span> Join Over <span>2500</span> Subscribers & Get Daily Reports Directly to Your Email
                            </h3>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-12 align-right">
                            <a href="" title="Subscribe" class="button biggest" style="font-size: 1.8em"
                               data-toggle="modal" data-target="#atm-modal">
                                <i class="fontello-icon icon-info-circled"></i>SUBSCRIBE
                            </a>
                        </div>
                    </div>
                </section>
            </div>


            <section class="section">
                <div class="row">
                    <div class="col-md-6 section-2">
                        <h1>Model Portfolio</h1>


                        <div class="chartSlider">
                            <div>
                                <div class="chart" id="container-column"></div>
                            </div>
                        </div>
                        <div id="auPortfolioChart" style="display: none" data-csv="{{json_encode($AUChart)}}"></div>
                        <div id="nzPortfolioChart" style="display: none" data-csv="{{json_encode($NZChart)}}"></div>

                    </div>
                    <div class="col-md-6 section-2">
                        <h1>Media Articles & Reviews</h1>

                        <div class="recent_posts_separate" style="width: 100%;">
                            <div class="blog_post ">
                                <div class="post_image">
                                    <div class="post_thumbnail">
                                        <a href="https://www.nzherald.co.nz/nz/news/article.cfm?c_id=1&objectid=12106664" title="Property Reached It’s Peak?" target="_blank">
                                            <img src="/img/img-1.png" alt="Property Reached It’s Peak?">
                                        </a>
                                    </div>
                                </div>
                                <div class="post_content">
                                    <ul class="post_content_meta">
                                        <li>NZ HERALD</li>
                                        <li><a target="_blank" href="https://www.nzherald.co.nz/nz/news/article.cfm?c_id=1&objectid=12106664" class="post_read_more">Read more</a></li>
                                    </ul>
                                    <h3 style="padding-left: 0px;">
                                        <a target="_blank" href="https://www.nzherald.co.nz/nz/news/article.cfm?c_id=1&objectid=12106664">ATM’s Property Views in NZ Herald</a>
                                    </h3>

                                    <p class="post_content_text">It is not a question of whether Auckland's house prices fall, but by how much: Analyst</p>
                                </div>
                            </div>
                            <div class="blog_post ">
                                <div class="post_image">
                                    <div class="post_thumbnail">
                                        <a href="https://www.thehappysaver.com/blog/for-all-the-share-investors" title="For All The Share Investors" target="_blank">
                                            <img src="/img/img-2.png" alt="For All The Share Investors">
                                        </a>
                                    </div>
                                </div>
                                <div class="post_content">
                                    <ul class="post_content_meta">
                                        <li>The Happy Saver.com</li>
                                        <li><a target="_blank" href="https://www.thehappysaver.com/blog/for-all-the-share-investors" class="post_read_more">Read more</a></li>
                                    </ul>
                                    <h3 style="padding-left: 0px;">
                                        <a target="_blank" href="https://www.thehappysaver.com/blog/for-all-the-share-investors">For All The Share Investors</a>
                                    </h3>

                                    <p class="post_content_text">Great review of our services by the HappySaver investment blog</p>
                                </div>
                            </div>
                        </div>

                        {{--@include('index._ytdNotes')--}}

                    </div>
                </div>
            </section>


            <div class="blockquote-container section">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3 class="section-title">What Our Members Are Saying</h3>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="testimonial wow fadeInUp-one">
                            <div class="testimonial-header">
                                <div class="testimonial-image">
                                </div>
                                <div class="testimonial-meta">
                                    <span class="testimonial-author">Ben Morrison</span>
                                    <span class="testimonial-job">Sydney, Australia</span>
                                </div>
                            </div>
                            <blockquote class="testimonial-quote">ATM strategy has been exceptional.
                            </blockquote>
                            <div class="testimonial-desc">
                                <p>ATM strategy has been exceptional. From the client service through to the continued
                                    medium to long term focus, it has allowed me to cut through the noise that can
                                    affect any investor and enabled me to achieve my personal goals and more importantly
                                    Increase my knowledge and competence. I look forward to continuing my journey with
                                    ATM for years to come.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="testimonial wow fadeInUp-one">
                            <div class="testimonial-header">
                                <div class="testimonial-image">
                                </div>
                                <div class="testimonial-meta">
                                    <span class="testimonial-author">George Hercus</span>
                                    <span class="testimonial-job">Palmerston North, New Zealand</span>
                                </div>
                            </div>
                            <blockquote class="testimonial-quote">I find ATM to be a really approachable adviser
                                for the ASX/NZX markets.
                            </blockquote>
                            <div class="testimonial-desc">
                                <p>I find ATM to be a really approachable adviser for the ASX/NZX markets. I
                                    regularly have discussions with them and can always contact them before making
                                    purchases. Their website is also extremely user friendly and I really like how they
                                    have broken their investments down into themes e.g. tourism boom.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="container_freetrial" style="background-color: #ddf0f6">
                <section class="section">
                    <div class="row row-inline align-middle">
                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <h3>
                                <span>15 Day Free Trial:</span>
                                Access our Reports & Recommendations free of charge with no obligation.
                            </h3>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-12 align-right">
                            <a href="/auth/register" class="button biggest" title="Sign up" style="font-size: 1.8em">
                                <i class="fontello-icon icon-info-circled"></i>SIGN UP
                            </a>
                        </div>
                    </div>
                </section>
            </div>

            {{-- <div class="container_freetrial">
                 <section class="section">
                     <div class="row row-inline align-middle">
                         <div class="col-lg-9 col-md-8 col-sm-12">
                             <h2>
                                 <span>FREE DAILY NEWSLETTER:</span> Get Daily Reports Directly to Your Email
                             </h2>
                         </div>
                         <div class="col-lg-3 col-md-4 col-sm-12 align-right">
                             <a href="" title="Subscribe" class="button biggest" style="font-size: 2em"
                                data-toggle="modal" data-target="#atm-modal">
                                 <i class="fontello-icon icon-info-circled"></i>SUBSCRIBE
                             </a>
                         </div>
                     </div>
                 </section>
             </div>--}}

        </main>
    </div>

    <div class="modal fade" id="registerModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <i class="iconic-button icon-cancel-circled" data-dismiss="modal" aria-hidden="true"></i>

            <div class="modal-content">
                <div class="header_modal form_group">
                    <h3>SIGN UP FOR A 15-DAY FREE TRIAL MEMBERSHIP</h3>
                    <h4>Gain access to our top stock ideas and portfolios<br>with a proven track record of performance
                    </h4>
                    <i class="close_modal icon-cancel-circle icon-cancel-circle pull-right" data-dismiss="modal"
                       aria-hidden="true"></i>
                </div>
                <form method="POST" action="/auth/register" id="payment-form" novalidate>
                    {!! csrf_field() !!}
                    <div class="form_group">
                        <input type="text" name="firstName" placeholder="Full Name*" value="{{ old('firstName') }}"
                               data-validation="notempty">
                    </div>
                    <!--<div class="form_group">
                        <input type="text" name="lastName" placeholder="Last Name*" value="empty" >
                    </div> -->
                    <div class="form_group">
                        <input type="email" name="email" value="{{ old('email') }}" placeholder="Email*"
                               data-validation="notempty;isemail">
                    </div>
                    <div class="form_group" style="display: none">
                        <input type="tel" name="dayTimePhone" value="{{ old('dayTimePhone') }}"
                               placeholder="Daytime Phone"
                               data-validation="isnumeric">
                    </div>
                    <div class="form_group">
                        <!--<p style="text-align: left; font-size: 12px; color: white; padding-left: 15px;">Please use 61XXXXXXXXX for AU and 64XXXXXXXXX for NZ-based phones.<br>We will send a verification code to the provided phone.</p> -->
                        <input type="tel" name="mobilePhone" value="{{ old('mobilePhone') }}" placeholder="Mobile*"
                               data-validation="notempty;isphone">
                    </div>
                    <div class="form_group">
                        <input type="password" name="password" value="{{ old('password') }}" placeholder="Password*"
                               data-validation="notempty;minlength:6">
                    </div>
                <textarea name="comment" value="{{ old('comment') }}" placeholder="Message"
                          style="display: none"></textarea>
                    {{--@include('partials.stripeForm')--}}
                    <input class="button-form" type="submit" id="register-form-submit-button" title="Send" value="Send">

                </form>
            </div>
        </div>
    </div>
@endsection