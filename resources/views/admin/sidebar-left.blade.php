<aside data-sidebar-left-nicescroll id="sidebar-left" class="sidebar-circle">
<!-- Start left navigation - profile shortcut -->
<div class="sidebar-content">
    <a href="#" class="close">&times;</a>
    <div class="media">
        <a class="pull-left has-notif avatar" data-ng-href="page-profile">
            <img src="http://img.djavaui.com/?create=50x50,4888E1?f=ffffff" alt="admin">
            <i class="online"></i>
        </a>
        <div class="media-body">
            <h4 class="media-heading">Hello, <span>Lee</span></h4>
            <small>Web Designer</small>
        </div>
    </div>
</div>
<!-- /.sidebar-content -->
<!--/ End left navigation -  profile shortcut -->
<!-- Start left navigation - menu -->
<ul data-collapse-menu class="sidebar-menu">
    <!-- Start navigation - dashboard -->
    <li class="active">
        <a ui-sref="dashboard.index" data-active-menu>
            <span class="icon"><i class="fa fa-home"></i></span>
            <span class="text">Dashboard</span>
            <span class="selected"></span>
        </a>
    </li>
    <!--/ End navigation - dashboard -->
    <!-- Start category apps -->
    {{-- <li class="sidebar-category">
        <span>Apps</span>
        <span class="pull-right"><i class="fa fa-globe"></i></span>
    </li> --}}
    <li class="active">
        <a ui-sref="home.index" data-active-menu>
            <span class="icon"><i class="fa fa-home"></i></span>
            <span class="text">Member home page</span>
            <span class="selected"></span>
        </a>
    </li>
    <!-- Start navigation - block -->
    <li class="submenu">
        <a href="javascript:void(0);">
            <span class="icon"><i class="fa fa-user"></i></span>
            <span class="text">Users</span>
            <span class="arrow"></span>
        </a>
        <ul>
            <li><a ui-sref="users.index" data-active-menu>All users</a></li>
            <!-- <li><a ui-sref="roles.index" data-active-menu>Roles</a></li>
            <li><a ui-sref="permissions.index" data-active-menu>Permissions</a></li> -->
        </ul>
    </li>
    <!--/ End navigation - block -->
    <!--/ End category apps -->
</ul>
<!-- /.sidebar-menu -->
<!--/ End left navigation - menu -->
<!-- Start left navigation - footer -->
<div class="sidebar-footer hidden-xs hidden-sm hidden-md">
    <a data-setting class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Setting"><i class="fa fa-cog"></i></a>
    <a data-fullscreen class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Fullscreen"><i class="fa fa-desktop"></i></a>
    <a data-lock-screen class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Lock Screen"><i class="fa fa-lock"></i></a>
    <a data-logout class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Logout"><i class="fa fa-power-off"></i></a>
</div>
<!-- /.sidebar-footer -->
<!--/ End left navigation - footer -->
</aside>
