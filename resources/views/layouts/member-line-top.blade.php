<div class="members_info">
    <div class="row row-inline align-middle section">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 align-left">
            <ul style="list-style-type: none; margin: 0; padding: 0; overflow: hidden;">
                <li style="float: left;"><a href="/member" title="Home" style="display: block; padding-right: 20px;">Home</a></li>
                <li style="float: left;"><a href="/newsletter" title="Daily newsletter" style="display: block; padding-right: 20px;">Daily Newsletter</a></li>
                <li style="float: left;"><a href="/member/report/investor/{!!$featured->id!!}" title="Chart of the Week" style="display: block; padding-right: 20px;">Chart of The Week</a></li>
                <li style="float: left;"><a href="/member/stock/{!!$focus->code!!}" title="Stock in Focus" style="display: block; padding-right: 20px;">Stock in Focus</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="clearfix toogle_container section">
    <button type="button" class="navbar-toggle toggle_menu" id="toggleMenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div>