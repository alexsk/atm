<div class="main-footer clearfix">
    <section class="section">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 ">

                <div class="widget-text-contact">
                    <h4>Contact</h4>
                    <ul>
                        <li>
                            <span style="color:#fff; font-weight: bold">ATM Research</span>
                        </li>
                        <li>
                            HSBC Building, Level 20
                        </li>
                        <li>
                            188 Quay Street
                        </li>
                        <li>
                            Auckland , New Zealand
                        </li>
                        <li></li>
                        <li>
                            Toll-Free: <a class="" title="0800 435 001" href="tel:0800 435 001">0800 435 001</a>
                        </li>
                        <li>
                            Email: info@atmstrategy.com.au
                        </li>
                        <li>
                            Financial Service Provider No. FSP774812
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="widget-text-three">
                    <h4>Follow Us</h4>

                    <div class="footer_social_media">
                        <a href="https://www.facebook.com/atmstrategy/" target="_blank" data-toggle="tooltip" title="Facebook"><i
                                    class="fontello-icon icon-facebook"></i></a>
                        <a href="https://twitter.com/ATMStrat" target="_blank" data-toggle="tooltip" title="Twitter"><i
                                    class="fontello-icon icon-twitter"></i></a>
                    </div>
                    <div class="widget-text-contact">
                        <h4>&nbsp;</h4>
                        <a href="/terms">Terms and Conditions</a>
                    </div>
                </div>
            </div>


        </div>
    </section>
</div>
<div class="lower-footer">
    <section class="section">
                <p>© 2021 ATM Research All Rights Reserved. The site contains investment reports, views, opinions, conclusions, estimates, recommendations and other information (Information). However, such Information comprises general securities information only, and has not been prepared taking into account the particular investment objectives, financial situation and needs of any particular person. Accordingly, you should assess whether it is appropriate in light of your individual circumstances or contact your financial planner or advisor.
                    Australasian Trading Partners Ltd is a NZ owned business: NZBN:9429041877449</p>
    </section>
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 935833399;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/935833399/?value=0&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
</div>