<div class="members_info">
    <div class="row row-inline align-middle section">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 align-left">
            <a href="/notes/{!!$newsletter[0]->slug!!}">
                <div class="members_info_left_column">
                    <span>Click to Read Latest Newsletter: <u>{!!$newsletter[0]->title!!}</u></span>
                </div>
            </a>
        </div>
    </div>
</div>
