<div id="anchor-landmark"></div>


<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq)return;
        n = f.fbq = function () {
            n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq)f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window,
            document, 'script', '//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1648644858708214');
    fbq('track', "PageView");
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=1648644858708214&ev=PageView&noscript=1"/>
</noscript>

<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/9929179ae5accc9ec2f52ee3b/26da14c4543c77e402a9c02c6.js");</script>

<div class="pre_header clearfix">
    <div class="section">
        <section class="row row-inline align-bottom ">
            <div class="col-lg-6 col-md-6 col-sm-6">
                @if(!auth()->check())
                    <a href="/index" title="ATM">
                        <img class="img-responsive" src="/img/logo.png" alt="logo" title="ATM"/>
                    </a>
                @else
                    <a href="/member" title="ATM">
                        <img class="img-responsive" src="/img/logo.png" alt="logo" title="ATM"/>
                    </a>
                @endif
                <small class="logo-small-text">
                </small>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="pre_header_right_column">
                    <a href="tel:0800 435 001" title="0800 435 001"><i
                                class="fontello-icon icon-phone-circled"></i><span>0800-435-001 (NZ)</span></a>
                    <a href="https://www.facebook.com/atmstrategy/" target="_blank" title="Facebook"><i
                                class="fontello-icon icon-facebook"></i></a>
                    <a href="https://twitter.com/ATMStrat" target="_blank" title="Twitter"><i
                                class="fontello-icon icon-twitter"></i></a>
                    @if(!auth()->check())
                        <a href="/auth/login" title="Member login" class="button button-member-login">Member Login</a>
                    @else
                        <a href="/auth/logout" title="Member logout" class="button button-member-login">Member
                            Logout</a>
                    @endif
                </div>
                <div class="mobile-button-auth">
                    @if(!auth()->check())
                        <a href="/auth/login" title="Member login" class="button button-member-login">Member Login</a>
                    @else
                        <a href="/auth/logout" title="Member logout" class="button button-member-login">Member
                            Logout</a>
                    @endif
                </div>
            </div>
        </section>
    </div>
</div>
<div class="lower_header">
    <div class="row section clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <nav role="navigation">
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav_header">
                        @if(!auth()->check())
                            <li><a href="/index" title="Home">Home</a></li>
                            <li><a href="/about" title="About ATM">ABOUT & EDUCATION</a></li>
                            <li><a href="/products" title="Products">products</a></li>
                            <li><a href="/notes" title="Notes">recent posts</a></li>
                            <li><a href="/pricing" title="Pricing">pricing</a></li>
                            <li><a href="/auth/register" title="Become a member">become a member</a></li>
                        @else
                            <li><a href="/member" title="Home">Home</a></li>
                            <li><a href="/about" title="About ATM">ABOUT & EDUCATION</a></li>
                        @endif
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>