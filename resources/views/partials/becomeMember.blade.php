<div class="products-container">
    <div class="section">
        <div class="row products-row ">
            <div class="products-column" style="font-size: 1.8em;">
                <h3>All Australasian Trading Management members receive the following features</h3>
            </div>
        </div>
    </div>

    <div class="section">
        <div class="row products-row ">
            <div class="col-lg-4 col-md-4 col-sm-4 products-column">

                <h3>MODEL PORTFOLIOS</h3>

                <div class="products-model-portfolios">
                    <p>
                        ATM Australian Equity Portfolio <br>
                        ATM NZ Equity Portfolio <br>
                        Dividend Portfolio
                    </p>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 products-column">

                <h3>DAILY MARKET INSIGHTS</h3>

                <p>
                    ATM's concise take on global market moves, trade recommendations and strategies.
                </p>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 products-column">

                <h3>Stock Specific Reports</h3>

                <p>
                    Coverage of the NZ, Australian and US Stock markets. From small cap to large, we cover a
                    range of companies across all industries.
                </p>

            </div>
        </div>
    </div>

<div class="section">
    <div class="row products-row ">
        <div class="col-lg-4 col-md-4 col-sm-4 products-column">

            <h3>TECHNICAL ANALYSIS</h3>

            <p>
                We provide technical (price) analysis based on past data to manage risk & identify
                potential
                entry and exit points, updated daily.
            </p>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 products-column">

            <h3>TOP TRADE REPORTS</h3>

            <p>
                ATM publishes proprietary top trade reports, which can cover a range of topical investment
                ideas.
            </p>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 products-column">

            <h3>INVESTOR EDUCATION</h3>

            <p>
                The financial world can be full of complicated jargon and ATM seeks to explain often
                overcomplicated concepts to its members in an easy to understand format.
            </p>

        </div>
    </div>
</div>

</div>