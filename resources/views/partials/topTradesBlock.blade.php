<div class="container-right-column">
    <a href="/newsletter">
        <h2 class="members_info_main_right_column_title" style="margin-bottom:5px;">ATM DAILY NEWSLETTER</h2>
        <img class="img-responsive" src="/img/envelopes.jpg" alt="ATM DAILY NEWSLETTER"></a>
</div>
<div class="container-right-column">
    <a href="/member/Dividend">
        <h2 class="members_info_main_right_column_title" style="margin-bottom:5px;">DIVIDEND PORTFOLIO</h2>
        <img class="img-responsive" src="/img/dividends.jpg" alt="DIVIDEND PORTFOLIO"></a>
</div>
<div class="container-right-column">
    <a href="/member/report/topTrades">
        <h2 class="members_info_main_right_column_title" style="margin-bottom:5px;">ATM TOP TRADES</h2>
        <img class="img-responsive" src="/img/products_market.jpg" alt="ATM TOP TRADES"></a>
</div>
