@extends('layouts.landing')
@section('title', 'Reset password ')
@section('content')
    <section class="section">
        <div class="container-password-recovery container-change-password">
            <div class="row row-inline align-middle">
                <div class="col-lg-6 col-md-6 col-sm-6 align-center">
                    <img src="/img/password_recovery.jpg" alt=""/>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger alert-server">
                            <strong>Whoops!</strong> There were some problems with your input.
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="clearfix" method="POST" action="/password/reset" novalidate>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <input type="hidden" name="token" value="{{ $token }}">
                        <h3>change your password</h3>
                        <ul>
                            <li>
                                Passwords are case-sensitive and must be at least 6 characters.
                            </li>
                            <li>
                                A good password should contain a mix of capital and lower-case letters, numbers and
                                symbols.
                            </li>
                        </ul>

                        <div class="form_group">
                            <input type="email" name="email" value="{{ old('email') }}" placeholder="Email"
                                    data-validation="notempty;isemail">
                        </div>

                        <div class="form_group">
                            <input type="password" name="password" placeholder="New Password"
                                   data-validation="notempty;passretry">
                        </div>

                        <div class="form_group">
                            <input type="password" name="password_confirmation" placeholder="Confirm New Password"
                                   data-validation="notempty;passretry">
                        </div>

                        <input class="button-form" type="submit" value="Change password"
                               name="change-password">

                    </form>
                </div>
            </div>

        </div>
    </section>
@endsection