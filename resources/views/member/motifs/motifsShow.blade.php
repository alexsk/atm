@extends('layouts.landing')
@section('title', 'Motifs Show')
@section('content')

    <main class="separate-motifs-container section">
        <div class="pre-header-separate-motifs">
            <img src="/img/trading.jpg" alt="">
        </div>
        <div class="main-separate-motifs">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                    <div class="separate-motifs-thumbnail">
                        <img src="/img/trading.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <div class="separate-motifs-meta">
                        <h3>invest in</h3>

                        <h2>header header header</h2>

                        <p class="separate-motifs-author">Created by: <a href="">Motif Investing</a></p>

                        <ul class="separate-motifs-value">
                            <li class="">
                                <span>-104%</span><small>Year to Date</small>
                            </li>
                            <li>
                                <span>74%</span><small>Year to Date</small>
                            </li>
                            <li>
                                <span>25%</span><small>Year to Date</small>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-10 col-lg-push-1 col-md-push-1 col-sm-push-1">
                    <h2 class="recent_posts_title">Lorem ipsum dolor sit amet</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-10 col-lg-push-1 col-md-push-1 col-sm-push-1">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ornare facilisis accumsan. Sed
                        tincidunt neque at elementum lacinia. Etiam fermentum neque massa, quis tincidunt lectus
                        dignissim in. Aenean elementum, eros eu lobortis pellentesque, mi nisl molestie est, vel egestas
                        velit tellus in erat. Nunc mollis sollicitudin elit eu auctor. Donec imperdiet congue sem. Class
                        aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas
                        vel eros at felis sodales porta a vitae lectus. Proin eu auctor libero. Aliquam pulvinar
                        dignissim augue nec sollicitudin. Nulla pretium nisi sem, at fringilla massa elementum vitae.
                        Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                        Curabitur efficitur, purus vel consequat feugiat, mauris tellus maximus erat, quis feugiat lacus
                        lorem vitae mi. Cras blandit eros eget libero malesuada, consequat malesuada odio dapibus. Fusce
                        feugiat ligula sit amet nisl sollicitudin, eu vehicula orci elementum.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-10 col-lg-push-1 col-md-push-1 col-sm-push-1">
                    <div class="motifs-separate-chart" id="motifs-separate-chart"></div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-10 col-lg-push-1 col-md-push-1 col-sm-push-1 motifs-table-container">


                    <table class="motifs-table">
                        <thead>
                        <tr>
                            <td>Weight</td>
                            <td>Segment & Stocks</td>
                            <td>Symbol</td>
                            <td>1 MO/1 YR Return</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="4">
                                <table class="motifs-table-separate">
                                    <thead data-toggle="collapse"
                                           data-target="#accordion-table-one">
                                    <tr>
                                        <th>28.1%</th>
                                        <th>Consumer Devices</th>
                                        <th></th>
                                        <th>12.1</th>
                                    </tr>
                                    </thead>
                                </table>

                                <div id="accordion-table-one" class="collapse in">
                                    <table class="motifs-table-separate accordion-table">
                                        <tr>
                                            <td>9.0%</td>
                                            <td>Garmin Ltd</td>
                                            <td>grmin</td>
                                            <td class="">10.0%</td>
                                        </tr>
                                        <tr>
                                            <td>7.2%</td>
                                            <td>Apple Inc</td>
                                            <td>aapl</td>
                                            <td>-16.6%</td>
                                        </tr>
                                        <tr>
                                            <td>6.8%</td>
                                            <td>FitBit Inc</td>
                                            <td>FiT</td>
                                            <td>-15.1%</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <table class="motifs-table-separate">
                                    <thead data-toggle="collapse"
                                           data-target="#accordion-table-two">
                                    <tr>
                                        <th>17.4%</th>
                                        <th>Consumer Devices</th>
                                        <th></th>
                                        <th>12.1</th>
                                    </tr>
                                    </thead>
                                </table>

                                <div id="accordion-table-two" class="collapse in">
                                    <table class="motifs-table-separate accordion-table">
                                        <tr>
                                            <td>19.0%</td>
                                            <td>Garmin Ltd</td>
                                            <td>grmin</td>
                                            <td>27.2%</td>
                                        </tr>
                                        <tr>
                                            <td>77.2%</td>
                                            <td>Apple Inc</td>
                                            <td>aapl</td>
                                            <td>16.6%</td>
                                        </tr>
                                        <tr>
                                            <td>6.8%</td>
                                            <td>FitBit Inc</td>
                                            <td>FiT</td>
                                            <td class="">-12.0%</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <table class="motifs-table-separate">
                                    <thead data-toggle="collapse"
                                           data-target="#accordion-table-three">
                                    <tr>
                                        <th>17.4%</th>
                                        <th>Consumer Devices</th>
                                        <th></th>
                                        <th>12.1</th>
                                    </tr>
                                    </thead>
                                </table>

                                <div id="accordion-table-three" class="collapse">
                                    <table class="motifs-table-separate accordion-table">
                                        <tr>
                                            <td>19.0%</td>
                                            <td>Garmin Ltd</td>
                                            <td>grmin</td>
                                            <td>27.2%</td>
                                        </tr>
                                        <tr>
                                            <td>77.2%</td>
                                            <td>Apple Inc</td>
                                            <td>aapl</td>
                                            <td>16.6%</td>
                                        </tr>
                                        <tr>
                                            <td>6.8%</td>
                                            <td>FitBit Inc</td>
                                            <td>FiT</td>
                                            <td>11.1%</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                        </tbody>

                    </table>

                </div>


            </div>
        </div>
    </main>



@endsection