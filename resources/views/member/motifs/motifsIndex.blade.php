@extends('layouts.landing')
@section('title', 'Motifs ')
@section('content')

    <main class="container-motifs">
        <div class="row section">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2 class="members_info_main_right_column_title">Top Poftfolio - here is why</h2>
            </div>
        </div>


        <div class="motifs-row">

            <div class="row section">

                <div class="col-lg-4 col-md-4 col-sm-6 motifs-column">
                    <figure class="effect-steve">
                        <a href="">
                            <div><img src="img/recent_picks_1.jpg" alt=""/></div>
        {{--                    <div class="motifs-figcaption">
                                <h3>common header</h3>
                                <h4>header</h4>
                                <p>Created by: <span>Author</span></p>
                            </div>--}}
                        </a>

                    </figure>
                    <div class="description_main_image">
                        <h4>ELDERS LTD</h4>

                        <div class="project-info">
                            <i class="fontello-icon icon-up-bold"></i><span>104% <small>Year to Date</small></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 motifs-column">
                    <figure class="effect-steve">
                        <a href="">
                            <div><img src="img/recent_picks_2.jpg" alt=""/></div>
            {{--                <div class="motifs-figcaption">
                                <h3>common header</h3>
                                <h4>header</h4>
                                <p>Created by: <span>Author</span></p>
                            </div>--}}
                        </a>

                    </figure>
                    <div class="description_main_image">
                        <h4>ELDERS LTD</h4>

                        <div class="project-info">
                            <i class="fontello-icon icon-up-bold"></i><span>104% <small>Year to Date</small></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 motifs-column">
                    <figure class="effect-steve">
                        <a href="">
                            <div><img src="img/recent_picks_3.jpg" alt=""/></div>
           {{--                 <div class="motifs-figcaption">
                                <h3>common header</h3>
                                <h4>header</h4>
                                <p>Created by: <span>Author</span></p>
                            </div>--}}
                        </a>

                    </figure>
                    <div class="description_main_image">
                        <h4>ELDERS LTD</h4>

                        <div class="project-info">
                            <i class="fontello-icon icon-up-bold"></i><span>104% <small>Year to Date</small></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 motifs-column">
                    <figure class="effect-steve">
                        <a href="">
                            <div><img src="img/recent_picks_1.jpg" alt=""/></div>
       {{--                     <div class="motifs-figcaption">
                                <h3>common header</h3>
                                <h4>header</h4>
                                <p>Created by: <span>Author</span></p>
                            </div>--}}
                        </a>

                    </figure>
                    <div class="description_main_image">
                        <h4>ELDERS LTD</h4>

                        <div class="project-info">
                            <i class="fontello-icon icon-up-bold"></i><span>104% <small>Year to Date</small></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 motifs-column">
                    <figure class="effect-steve">
                        <a href="">
                            <div><img src="img/recent_picks_2.jpg" alt=""/></div>
            {{--                <div class="motifs-figcaption">
                                <h3>common header</h3>
                                <h4>header</h4>
                                <p>Created by: <span>Author</span></p>
                            </div>--}}
                        </a>

                    </figure>
                    <div class="description_main_image">
                        <h4>ELDERS LTD</h4>

                        <div class="project-info">
                            <i class="fontello-icon icon-up-bold"></i><span>104% <small>Year to Date</small></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 motifs-column">
                    <figure class="effect-steve">
                        <a href="">
                            <div><img src="img/recent_picks_3.jpg" alt=""/></div>
           {{--                 <div class="motifs-figcaption">
                                <h3>common header</h3>
                                <h4>header</h4>
                                <p>Created by: <span>Author</span></p>
                            </div>--}}
                        </a>

                    </figure>
                    <div class="description_main_image">
                        <h4>ELDERS LTD</h4>

                        <div class="project-info">
                            <i class="fontello-icon icon-up-bold"></i><span>104% <small>Year to Date</small></span>
                        </div>
                    </div>
                </div>


            </div>

        </div>



    </main>

@endsection