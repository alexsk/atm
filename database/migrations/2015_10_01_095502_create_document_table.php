<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('author')->default('unknown');
            $table->string('name');
            $table->string('type');
            $table->string('category');
            $table->integer('categoryId')->unsigned()->default(0)->nullable();
            $table->string('status');
            $table->string('path')->unique();
            $table->string('documentId')->unique();
            $table->string('viewUrl');
            $table->string('assetsUrl');
            $table->string('realtimeUrl');
            $table->dateTime('expiresAt')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documents');
    }
}
