<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('dayTimePhone');
            $table->string('mobilePhone');
            $table->string('postcode');
            $table->string('sessionId');
            $table->string('userHash')->nullable();
            $table->string('ip', 45);
            $table->mediumText('comment');
            $table->enum('listenFrom', ['internet', 'friends', 'family'])->default('internet');
            $table->enum('status', ['trial', 'full', 'blocked', 'admin', 'notActive', 'subscriber'])->default('notActive');
            $table->enum('isOnline', ['Yes', 'No'])->default('No');
            $table->rememberToken();
            $table->string('activation_code')->nullable();
            $table->string('transaction_subject')->nullable();
            $table->string('payer_email')->nullable();
            $table->dateTime('payment_date')->nullable();
            $table->timestamps();
            $table->index('payer_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
