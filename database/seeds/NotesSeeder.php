<?php

use App\Models\Note;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class NotesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $faker = (new FakerGenerator())->generate();

        /*$path = public_path().'/uploads/notes/' . (1);
        \File::makeDirectory($path);
        $imagePath = $faker->image($dir = $path, $width = 640, $height = 640, 'cats', true, 'Faker');
        $tokens = explode('/', $imagePath);
        Note::create([
            'title' => "Treasury Wine Estates (TWE.ASX)",
            'quote' => "TWE is beginning to reap the rewards from its attractive investment opportunities…",
            'body' => '<p><img alt="" src="http://i.imgur.com/0lELd2k.png"/></p><p>TWE is beginning to reap the rewards from its attractive investment opportunities. The share price is up almost 20% in the past 3 months with its expansion into Asia maturing. The company reported strong financial results for FY15 with profit up 22% and sales up 8.4%. Its key growth markets are set to continue to drive the strong performance of the company into the future and&hellip;</p>',
            'image' => '/uploads/notes/' . (1) . '/' . $tokens[sizeof($tokens)-1],
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
        ]);

        $path = public_path().'/uploads/notes/' . (2);
        \File::makeDirectory($path);
        $imagePath = $faker->image($dir = $path, $width = 640, $height = 640, 'cats', true, 'Faker');
        $tokens = explode('/', $imagePath);
        Note::create([
            'title' => "ELDERS LTD (ELD.ASX)",
            'quote' => "ELD’s has undergone a successful business restructure and economic factors have aligned for the Australian based agricultural business…",
            'body' => '<p><img alt="" src="http://i.imgur.com/HHewNIZ.png"/></p>
            <p>We are positively positioned towards the agricultural theme over the longer term given growing food demand forecast to come out of the developing economies, particularly in Asia. After facing financial difficulties, ELD has managed to restructure the company, and is now focussed on an 8 point 3 year operating plan. Our view is that while there are always implementation risks, ELD looks to be on track with these initiatives such as capital cost savings and brand development. At the same there have been a number of positive external developments for ELD including the introduction of new free trade agreements between Australia and Asian countries, improved weather conditions, strong cattle and sheep prices, and a lower Australian dollar. Give the stock has had such a strong run&hellip;</p></p>',
            'image' => '/uploads/notes/' . (2) . '/' . $tokens[sizeof($tokens)-1],
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
        ]);

        $path = public_path().'/uploads/notes/' . (3);
        \File::makeDirectory($path);
        $imagePath = $faker->image($dir = $path, $width = 640, $height = 640, 'cats', true, 'Faker');
        $tokens = explode('/', $imagePath);
        Note::create([
            'title' => "FISHER & PAYKEL HEALTHCARE (FPH.NZX)",
            'quote' => "FPH has experienced strong operational improvements from new products which have been years in the making…",
            'body' => '<p><img alt="" src="http://i.imgur.com/O5DdpwZ.png" /></p><p>Fisher & Paykel Healthcare (FPH.NZ) has been one of the main beneficiaries of a lower New Zealand dollar, as almost all of its revenues are generated outside of New Zealand. As we have highlighted previously this has certainly not been lost on the market, with the FPH share price up another 29% this year alone. However currency moves aside, FPH has experienced strong operational improvements from new products which have been years in the making. At its recent investor day FPH highlighted the elevated long term growth potential for the business going forward…</p>',
            'image' => '/uploads/notes/' . (3) . '/' . $tokens[sizeof($tokens)-1],
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
        ]);

        $path = public_path().'/uploads/notes/' . (4);
        \File::makeDirectory($path);
        $imagePath = $faker->image($dir = $path, $width = 640, $height = 640, 'cats', true, 'Faker');
        $tokens = explode('/', $imagePath);
        Note::create([
            'title' => "AIR NEW ZEALAND (AIR.NZX)",
            'quote' => "AIR’s performance has been driven by growth in its passenger capacity, operating efficiencies and the New Zealand tourism sector…",
            'body' => '<p><img alt="" src="http://imgur.com/nK9itti" /></p><p>AIR has performed strongly over the past 3 months returning almost 16% to shareholders. The impressive performance has been driven by major growth in its passenger capacity, operating efficiencies and continual improvements to the New Zealand tourism sector which is benefiting from a falling NZD. ATM is was encouraged by the company’s latest trading update and is...</p>',
            'image' => '/uploads/notes/' . (4) . '/' . $tokens[sizeof($tokens)-1],
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
        ]);

        $path = public_path().'/uploads/notes/' . (5);
        \File::makeDirectory($path);
        $imagePath = $faker->image($dir = $path, $width = 640, $height = 640, 'cats', true, 'Faker');
        $tokens = explode('/', $imagePath);
        Note::create([
            'title' => "An Australasian Agricultural Boom",
            'quote' => "ATM’s NZ and Australian portfolios have large positions in the Agricultural sector, and our preferred agriculture stocks have performed strongly this year. We see a “Dining boom” replacing Australia’s “Mining Boom” and becoming the next major multi-year theme for Australasia.",
            'body' => '<p><img alt="" src="http://i.imgur.com/xhV3XTI.png"/></p><p>ATM&rsquo;s NZ and Australian portfolios have large positions in the Agricultural sector, and our preferred agriculture stocks have performed strongly this year. We see a &ldquo;Dining boom&rdquo; replacing Australia&rsquo;s &ldquo;Mining Boom&rdquo; and becoming the next major multi-year theme for Australasia. Demand for a higher protein diet from a growing middle class in the developing world (in particular Asia) is set to be a multi-year investment theme. Australasia is set to directly benefit from this dynamic, with Australia and New Zealand being exporting nations in close proximity to Asia. Our portfolios have large overweight positions focused on Australasian agricultural businesses such as Elders (ELD.ASX) which is up a massive 104% year to date&hellip;</p>',
            'image' => '/uploads/notes/' . (1) . '/' . $tokens[sizeof($tokens)-1],
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
        ]);

        $path = public_path().'/uploads/notes/' . (6);
        \File::makeDirectory($path);
        $imagePath = $faker->image($dir = $path, $width = 640, $height = 640, 'cats', true, 'Faker');
        $tokens = explode('/', $imagePath);
        Note::create([
            'title' => 'Making mountains out of mining',
            'quote' => 'With mining on the decline ATM remains bearish on the sector.  Since September 2013 the Australian market (ASX 200) is down -3.8%, while the ASX 200 Resources sector is down -35.4%, with most of the fall occurring over the last year. Instead ATM is focusing on sunrise industries set to give investors significant returns.',
            'body' => '<p><img alt="" src="http://i.imgur.com/U0ht2B4.png" /></p><p>ATM portfolio has benefited significantly from remaining firmly underweight resources in Australia. The resource sector in Australia has been a major drag on stocks. The slowdown in China has been the major catalyst for this correction. China&rsquo;s slowing growth rate translates to a significantly lower demand for resources and materials. Major raw materials such as iron ore, copper and oil have all experienced significant price disruptions as the lower forecast demand is factored into current pricing. ATM remains significantly underweight the resource sector given the current bearish outlook. We have a number of stocks that we are currently monitoring and are awaiting more attractive valuations before entering a position&hellip;</p>',
            'image' => '/uploads/notes/' . (2) . '/' . $tokens[sizeof($tokens)-1],
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
        ]);*/
    }

}
