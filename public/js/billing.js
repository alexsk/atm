$(document).ready(function() {
    var StripeBilling = {
        init: function() {
            this.form = $('#payment-form');
            this.submitButton = this.form.find('input[type=submit]');
            this.submitButtonValue = this.submitButton.val();

            var stripeKey = $("#publishable-key").val();
            Stripe.setPublishableKey(stripeKey);

            this.bindEvents();
        },

        bindEvents: function() {
            this.form.on('submit', $.proxy(this.sendToken, this));
        },

        sendToken: function(event) {
            var that = this;
            event.preventDefault();
            window.setTimeout(function() {
                if (typeof PrettyForms != "undefined") {
                    var errors = $('.prettyforms-validation-error').length;
                    console.log(errors);
                    if (errors) {
                        event.preventDefault();
                        return null;
                    } else {
                        return createToken();
                    }
                } else {
                    return createToken();
                }
            }, 200);

            function createToken() {
                that.submitButton.val('Please Wait ...').prop('disabled', true);
                Stripe.createToken(that.form, $.proxy(that.stripeResponseHandler, that));
            }
        },

        stripeResponseHandler: function(status, response) {
            if (response.error) {
                this.form.find('.payment-errors').show().text(response.error.message);
                return this.submitButton.prop('disabled', false).val(this.submitButtonValue);
            }
            console.log(response);

            $('<input>', {
                type: 'hidden',
                name: 'stripe-token',
                value: response.id
            }).appendTo(this.form);

            return this.form[0].submit();
            //return this.submitButton.prop('disabled', false).val(this.submitButtonValue);
        }
    };
    StripeBilling.init();
});
