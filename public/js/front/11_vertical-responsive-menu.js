/*
 ==========================
 Vertical Responsive Menu
 ==========================
 */

'use strict';

$('.js-menu').each(function () {
    var tid = setInterval( function () {
        if ( document.readyState !== 'complete' ) return;
        clearInterval( tid );


    /*    var querySelector = document.querySelector.bind(document);*/

        var nav = $('.vertical_nav');
   /*     var wrapper = $('.vertical_nav');*/

        var menu = $('.js-menu');
        var subnavs = menu.find('.menu--item__has_sub_menu');


        // Toggle menu click
        $('.toggle_menu').on('click', function() {
            $(this).parent('.toogle_container').toggleClass('toggle-content');
            nav.toggleClass('vertical_nav__opened');


        });


        // Minify menu on menu_minifier click
        $('.collapse_menu').onclick = function () {

            nav.toggleClass('vertical_nav__minify');

            wrapper.classList.toggle('wrapper__minify');

            for (var j = 0; j < subnavs.length; j++) {

                subnavs[j].classList.remove('menu--subitens__opened');

            }

        };


        var mq = window.matchMedia( "(max-width: 1200px)" );
        if (mq.matches){
            $(function() {
                $('.members_info_container').is( function() {
                    $(this).find('nav.vertical_nav').addClass('vertical_nav__opened');
                });
            });
        }





        $('.menu--item__has_sub_menu > .menu--link').on('click', function(){
            $(this).parent().toggleClass('menu--subitens__opened');
            nav.toggleClass('vertical_nav__min');
        });

        $('.menu--subitens__close').each(function () {
            $(this).find('.menu--item__has_sub_menu:not(:last-child)').toggleClass('menu--subitens__opened');
            nav.toggleClass('vertical_nav__min');
        })

        $('.menu--subitens__close_thematics').each(function () {
            $(this).find('.menu--item__has_sub_menu:not(:first-child)').toggleClass('menu--subitens__opened');
            nav.toggleClass('vertical_nav__min');
        })

    }, 100 );
})







