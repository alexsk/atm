(function() {
    'use strict';

    angular
        .module('app.users')
        .controller('UsersFormCtrl', UsersFormCtrl);

    UsersFormCtrl.$inject = ['users.model', 'logger', 'users.form', '$stateParams'];

    function UsersFormCtrl(model, logger, FormService, $stateParams) {
        var vm = this;
        var user = null;
        vm.listenFromOptions = [{
            label: 'Internet',
            value: 'internet'
        }, {
            label: 'Family',
            value: 'family'
        }, {
            label: 'Friends',
            value: 'friends'
        }];
        vm.statusOptions = [{
            label: 'Trial',
            value: 'trial'
        }, {
            label: 'Full 6 Months',
            value: 'full6'
        }, {
            label: 'Full 12 Months',
            value: 'full12'
        }, {
            label: 'Subscriber',
            value: 'subscriber'
        }, {
            label: 'Blocked',
            value: 'blocked'
        }, {
            label: 'Manager',
            value: 'manager'
        }, {
            label: 'Administrator',
            value: 'admin'
        }];

        vm.dateOptions = {
            dateDisabled: vm.disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };
        vm.formData = {};
        vm.submitFailed = submitFailed;
        vm.validationFailed = validationFailed;
        vm.openDatepicker = openDatepicker;
        vm.altInputFormats = ['M!/d!/yyyy'];
        vm.opened = false;
        vm.format = 'dd-MM-yyy';
        vm.disabled = disabled;

        activate();

        function activate() {
            if ($stateParams.id) {
                vm.formData = model.edit($stateParams.id);
                vm.title = 'Edit user';
            } else {
                vm.formData = model.create();
                vm.title = 'Create user';
            }
            vm.validationRules = FormService.getValidationRules();
            vm.submit = vm.validationRules.submit;
        }

        function validationFailed() {
            return logger.error("validaton faliled");
        }

        function submitFailed(error) {
            logger.error(error);
        }

        function openDatepicker() {

            vm.opened = true;
        }

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }
    }
})();
