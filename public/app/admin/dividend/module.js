(function() {
    'use strict';

    angular
        .module('app.dividend', [])
        .run(appRun);

    appRun.$inject = ['routehelper'];

    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return {
            'dividend': {
                url: '',
                template: '<div ui-view></div>',
                abstract: true
            },
            'dividend.index': {
                url: '/dividend',
                parent: 'dividend',
                templateUrl: '/app/admin/dividend/html/dividend.html',
                controller: 'Dividend as vm',
                title: 'Dividend Portfolio Page',
                data: {
                    nav: 1,
                    pageTitle: 'Dividend Portfolio',
                    pageHeader: {
                        icon: 'fa fa-home',
                        title: 'Dividend Portfolio Page',
                        subtitle: 'Dividend Portfolio Page'
                    },
                    breadcrumbs: [{
                        title: 'Portfolio'
                    }, {
                        title: 'Dividend Portfolio Page'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    '/js/plugins/ckeditor/contents.css',
                                ]
                            }, {
                                name: 'ngCkeditor',
                                files: [
                                    '/js/plugins/ckeditor/ckeditor.js',
                                    pluginPath + '/ng-ckeditor/ng-ckeditor.min.js',
                                ]
                            }, {
                                name: 'app.dividend',
                                files: [
                                    '/app/admin/dividend/ctrl.js'
                                ]
                            }]
                        );
                    }]
                }
            }
        };
    }
})();
