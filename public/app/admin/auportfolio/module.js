(function() {
    'use strict';

    angular
        .module('app.auportfolio', [])
        .run(appRun);

    appRun.$inject = ['routehelper'];

    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return {
            'auportfolio': {
                url: '',
                template: '<div ui-view></div>',
                abstract: true
            },
            'auportfolio.index': {
                url: '/auportfolio',
                parent: 'auportfolio',
                templateUrl: '/app/admin/auportfolio/html/auportfolio.html',
                controller: 'AUPortfolioCtrl as vm',
                title: 'Member auportfolio page',
                data: {
                    nav: 1,
                    pageTitle: 'auportfolio',
                    pageHeader: {
                        icon: 'fa fa-home',
                        title: 'member auportfolio page',
                        subtitle: 'member auportfolio page'
                    },
                    breadcrumbs: [{
                        title: 'Member'
                    }, {
                        title: 'member auportfolio page'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/angular-xeditable/dist/css/xeditable.css',
                                    '/js/plugins/ckeditor/contents.css',
                                    pluginPath + '/v-accordion/dist/v-accordion.min.css'
                                ]
                            }, {
                                name: 'ngFileUpload',
                                files: [
                                    pluginPath + '/ng-file-upload/ng-file-upload-all.min.js'
                                ]
                            }, {
                                name: 'xeditable',
                                files: [
                                    pluginPath + '/angular-xeditable/dist/js/xeditable.js'
                                ]
                            }, {
                                name: 'vAccordion',
                                files: [
                                    pluginPath + '/v-accordion/dist/v-accordion.min.js'
                                ]
                            }, {
                                name: 'ngCkeditor',
                                files: [
                                    '/js/plugins/ckeditor/ckeditor.js',
                                    pluginPath + '/ng-ckeditor/ng-ckeditor.min.js',
                                ]
                            }, {
                                name: 'app.auportfolio',
                                files: [
                                    '/app/admin/auportfolio/ctrl.js'
                                ]
                            }]
                        );
                    }]
                }
            }
        };
    }
})();
