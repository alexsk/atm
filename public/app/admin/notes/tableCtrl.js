(function() {
    'use strict';

    angular.module('app.notes').controller('NotesTableCtrl', NotesTableCtrl);

    NotesTableCtrl.$inject = ['notes.model', 'logger'];

    function NotesTableCtrl(model, logger) {
        /*jshint validthis: true */
        var vm = this;
        vm.fields = ['id', 'title', 'quote', 'created_at', 'updated_at'];
        vm.del = del;
        vm.tableActions = tableActions;
        vm.getAll = getAll;
        vm.modalMessage = "Are you sure?";

        activate();

        function activate() {
            vm.title = 'All notes';
            getAll();
        }

        function getAll() {
            vm.tableItems = model.all();
        }

        function tableActions(currentUser, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
            vm.tableItems = Notes.byUser(currentUser, pageItems, filterBy, filterByFields, orderBy, orderByReverse);
            vm.tableItems.$then(function(_collection) {
                vm.totalItems = _collection.$metadata.totalItems;
                vm.pageItems = _collection.$metadata.limit;
                vm.currentPage = _collection.$metadata.pageNumber ? _collection.$metadata.pageNumber : 0;
            });
            logger.info('Notes loaded');
        }

        function del(gridItem) {
            if (window.confirm(vm.modalMessage)) {
                model.del(gridItem.id);
                vm.tableItems.splice(vm.tableItems.indexOf(gridItem), 1);
            }
        }
    }
})();
