(function() {
    'use strict';

    angular
        .module('app.notes', [])
        .run(appRun);

    appRun.$inject = ['routehelper'];
    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return {
            'notes': {
                url: '',
                template: '<div ui-view></div>',
                abstract: true
            },
            'notes.index': {
                url: '/notes',
                templateUrl: '/app/admin/notes/html/table.html',
                controller: 'NotesTableCtrl as vm',
                title: 'Notes',
                parent: 'notes',
                data: {
                    nav: 11,
                    pageTitle: 'Notes',
                    pageHeader: {
                        icon: 'fa fa-sticky-note-o',
                        title: 'Notes',
                        subtitle: 'all notes'
                    },
                    breadcrumbs: [{
                        title: 'Notes'
                    }, {
                        title: 'All notes'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    //pluginPath + '/datatables/media/css/dataTables.bootstrap.min.css',
                                    //pluginPath + '/datatables-responsive-helper/files/1.10/css/datatables.responsive.css',
                                    //pluginPath + '/fuelux/dist/css/fuelux.min.css'
                                ]
                            }, {
                                name: 'app.notes',
                                files: [
                                    '/app/admin/notes/tableCtrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/notes/model.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'notes.create': {
                url: '/notes/create',
                templateUrl: '/app/admin/notes/html/form.html',
                controller: 'NotesFormCtrl as vm',
                title: 'Notes.Create',
                parent: 'notes',
                data: {
                    nav: 0,
                    pageTitle: 'Notes',
                    pageHeader: {
                        icon: 'fa fa-note-o',
                        title: 'Notes',
                        subtitle: 'create note'
                    },
                    breadcrumbs: [{
                        title: 'Create'
                    }, {
                        title: 'Create new note'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.css',
                                ]
                            }, {
                                name: 'formFor.bootstrapTemplates',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.bootstrap-templates.js'
                                ]
                            }, {
                                name: 'ngCkeditor',
                                files: [
                                    '/js/plugins/ckeditor/ckeditor.js',
                                    pluginPath + '/ng-ckeditor/ng-ckeditor.min.js',
                                ]
                            }, {
                                name: 'app.notes',
                                files: [
                                    '/app/admin/notes/formCtrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/notes/model.js',
                                    '/app/admin/notes/formData.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'notes.edit': {
                url: '/notes/:id/edit',
                templateUrl: '/app/admin/notes/html/form.html',
                controller: 'NotesFormCtrl as vm',
                title: 'Edit note',
                parent: 'notes',
                data: {
                    nav: 0,
                    pageTitle: 'Notes',
                    pageHeader: {
                        icon: 'fa fa-note',
                        title: 'Notes',
                        subtitle: 'edit note'
                    },
                    breadcrumbs: [{
                        title: 'Edit'
                    }, {
                        title: 'Edit note'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.css'
                                ]
                            }, {
                                name: 'formFor.bootstrapTemplates',
                                files: [
                                    pluginPath + '/angular-form-for/dist/form-for.bootstrap-templates.js'
                                ]
                            }, {
                                name: 'ngCkeditor',
                                files: [
                                    '/js/plugins/ckeditor/ckeditor.js',
                                    pluginPath + '/ng-ckeditor/ng-ckeditor.min.js',
                                ]
                            }, {
                                name: 'app.notes',
                                files: [
                                    '/app/admin/notes/formCtrl.js'
                                ]
                            }, {
                                name: 'app.data',
                                files: [
                                    '/app/admin/notes/model.js',
                                    '/app/admin/notes/formData.js'
                                ]
                            }]
                        );
                    }]
                }
            },
            'notes.image': {
                url: '/notes/:id/image',
                templateUrl: '/app/admin/notes/html/image.html',
                controller: 'NoteFilesCtrl as vm',
                title: 'Note files',
                parent: 'notes',
                data: {
                    nav: 0,
                    pageTitle: 'Note files',
                    pageHeader: {
                        icon: 'fa fa-file-pdf-o',
                        title: 'Notes',
                        subtitle: 'files note'
                    },
                    breadcrumbs: [{
                        title: 'File'
                    }, {
                        title: 'Note file'
                    }]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [{
                                insertBefore: '#load_css_before',
                                files: [
                                    pluginPath + '/ng-img-crop/compile/minified/ng-img-crop.css'
                                ]
                            }, {
                                name: 'ngImgCrop',
                                files: [
                                    pluginPath + '/ng-img-crop/compile/minified/ng-img-crop.js'
                                ]
                            }, {
                                name: 'ngFileUpload',
                                files: [
                                    pluginPath + '/ng-file-upload/ng-file-upload-all.min.js'
                                ]
                            }, {
                                name: 'app.notes',
                                files: [
                                    '/app/admin/notes/fileCtrl.js'
                                ]
                            }]
                        );
                    }]
                }
            }
        };
    }
})();
