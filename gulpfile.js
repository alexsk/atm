var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var useref = require('gulp-useref');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var fixmyjs = require("gulp-fixmyjs");
var critical = require('critical');
var tinypng = require('gulp-tinypng-compress');
var watch = require('gulp-watch');
var gaze = require('gaze');
var notify = require("gulp-notify");
var imagemin = require('gulp-imagemin');
var imageminOptipng = require('imagemin-optipng');
var imageminJpegtran = require('imagemin-jpegtran');
var pngquant = require('imagemin-pngquant');
var plumber = require('gulp-plumber');
var newer = require('gulp-newer');


gulp.task('cssFrontend', function () {
    gulp.src('public/css/style/*.css')
        .pipe(plumber())
        .pipe(autoprefixer())
        .pipe(concatCss("frontend.min.css"))
        .pipe(minifyCss())
        .pipe(useref())
        .pipe(gulp.dest('public/css'));
});


gulp.task('cssAdmin', function () {
    gulp.src('public/css/style/admin-style/*.css')
        .pipe(plumber())
        .pipe(autoprefixer())
        .pipe(minifyCss())
        .pipe(useref())
        .pipe(gulp.dest('public/css'));
});

gulp.task('scripts', function () {
    return gulp.src('public/js/front/*.js')
        .pipe(plumber())
        .pipe(jshint())
        .pipe(fixmyjs())
        .pipe(uglify())
        .pipe(concat('frontend.min.js'))
        .pipe(gulp.dest('public/js'));
});

gulp.task('tinypng', function () {
    gulp.src('public/img/1/*.*')

    .pipe(tinypng({
            key: 'vCmzJf0t8pPkrLGaaEHO9Sc0pehpHPlV',
            sigFile: 'public/img/2/.tinypng-sigs',
            log: true

        }))
        .pipe(gulp.dest('public/img/2'));

});

gulp.task('default', ['scripts', 'cssFrontend', 'cssAdmin', 'images', 'watch']);


gulp.task('watch', function () {
     gulp.watch('public/css/style/*.css', ['cssFrontend']);
     gulp.watch('public/css/style/admin-style/*.css', ['cssAdmin']);
     gulp.watch('public/js/front/*.js', ['scripts']);
     gulp.watch('public/uploads/*/*', ['images']);
});

// Images
gulp.task('images', function() {
    return gulp.src('public/uploads/*/*/*.{png,gif,jpg,jpeg,svg}')
        .pipe(newer('public/uploads-min/uploads'))
        .pipe(plumber())
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, use: [pngquant(), imageminOptipng({optimizationLevel: 3}), imageminJpegtran({progressive: true})] }))
        .pipe(gulp.dest('public/uploads-min/uploads'))
        .pipe(notify({ message: 'Images task complete' }))

});











