<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Document
 *
 * @property string $id
 * @property string $title
 * @property string $type
 * @property string $body
 * @property string $topLeft
 * @property string $topRight
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereTopLeft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereTopRight($value)
 * @mixin \Eloquent
 */
class Post extends Model {

	protected $table = 'posts';

}
