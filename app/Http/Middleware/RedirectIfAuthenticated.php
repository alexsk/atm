<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     * @param  Guard  $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (($request -> requestUri  == "/member/renew") || (strpos($_SERVER['REQUEST_URI'], 'upgradeTrial') !== false))
            return $next($request);
        if ($this->auth->check()) {
            if (me()->isAdmin()) {
                return redirect('/admin/#/home');
            } elseif (me()->isBlocked()) {
                return redirect('/blocked');
            } elseif(!me()->isActive()) {
                /*\Auth::logout();
                return redirect('/auth/login');*/
                return redirect('/become-member');
            } else {
                return redirect('/disclaimer'); //temporary! todo fix request null
            }
        }

        return $next($request);
    }
}
