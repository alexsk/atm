<?php namespace App\Http\Controllers\Member;

use App\Models\Document;
use App\Models\Stock;
use App\Models\Report;

class StockController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {

    }


    public function index($region = 'au')
    {
        $stocks = Stock::where('region', $region)->orderBy('name', 'asc')->paginate(100);

        return view('member.stock.index')
            ->with('stocks', $stocks);
    }

    public function show($code, $docId = null){
        $stock = Stock::where('code',$code)->first();
        if(!$stock){
            return redirect()->back();
        }
        $documents = Document::where('category', 'stocks')->where('categoryId', $stock->id)->orderBy('id', 'DESC')->get();
        $doc = null;
        if(!$docId){
            $doc = $documents->first();
        }else {
            $doc = $documents->filter(function ($item) use ($docId) {
                return $item->id == $docId;
            })->first();
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.box.com/2.0/files/" . $doc->newBoxId . "?fields=expiring_embed_link",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer WwdsQPRT6nDIiyA53SNsl5wMD6PXWKE2",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $result = json_decode($response, true);
            $doc->viewUrl = $result["expiring_embed_link"]["url"] ."?showDownload=true";
            $doc->save();
        }

        return view('member.stock.show')->with('stock', $stock)
            ->with('doc', $doc)
            ->with('documents', $documents);
    }

}