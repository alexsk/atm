<?php namespace App\Http\Controllers\Member;

use App\Models\Document;
use App\Models\Note;
use App\Models\Post;
use App\Models\Report;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use DateTime;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {

    }

    public function index()
    {
        $document = Document::where('path', 'member/home/forHomePage.pdf')->first();
        $topTrades = null;
        $chart = null;
        if (Storage::exists('member/home/topTrades.csv')) {
            $topTrades = Reader::createFromPath(storage_path('app/member/home/topTrades.csv'));
        }
        if (Storage::exists('member/home/chartData.csv')) {
            $chart = Reader::createFromPath(storage_path('app/member/home/chartData.csv'));
        }
        $ytdNotes = Note::take(2)->where('pick', '1')->where('newsletter','<>','1')->latest()->get();

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.box.com/2.0/files/". $document -> newBoxId. "?fields=expiring_embed_link",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer WwdsQPRT6nDIiyA53SNsl5wMD6PXWKE2",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $result = json_decode($response, true);
            $document->viewUrl = $result["expiring_embed_link"]["url"];
            $document->save();
        }

        return view('member.home')
            ->with('topTrades', $topTrades ? $topTrades->fetchAll() : null)
            ->with('chart', $chart ? $chart->fetchAll() : null)
            ->with('ytdNotes', ($ytdNotes->count() == 2) ? $ytdNotes : null)
            ->with('document', $document ? $document : null);

    }

    public function AUPortfolio()
    {
        $csvs = [];
        $names = ['topTenHoldings', 'regionalBreakdown', 'sectorBreakdown', 'lastRight', 'chartOfMoment', 'newZelandPortfolioPerformance'];
        foreach ($names as $name) {
            $path = 'member/AUPortfolio/' . $name . '.csv';
            if (Storage::exists($path)) {
                $csvs[$name] = Reader::createFromPath(storage_path('app/' . $path))->fetchAll();
            } else {
                $csvs[$name] = null;
            }
        }
        $text = Post::where('type', 'AUPortfolio')->where('title', 'AUPortfolio')->first();
        $trial = "false";
        if (me()->isTrial() && me()->payment_date > DateTime::createFromFormat('j-M-Y', '3-Feb-2016')) {
            $trial = "true";
        }


        return view('member.auportfolio')->with('csvs', $csvs)->with('text', $text)
            ->with('trial', $trial)
            ->with('chart', $csvs['chartOfMoment'] ? $csvs['chartOfMoment'] : null)
            ->with('topTen', $csvs['topTenHoldings'] ? $csvs['topTenHoldings'] : null)
            ->with('thematicExpsoure', $csvs['sectorBreakdown'] ? $csvs['sectorBreakdown'] : null)
            ->with('industryWeightings', $csvs['regionalBreakdown'] ? $csvs['regionalBreakdown'] : null);
    }

    public function NZPortfolio()
    {
        $csvs = [];
        $names = ['topTenHoldings', 'regionalBreakdown', 'sectorBreakdown', 'lastRight', 'chartOfMoment', 'newZelandPortfolioPerformance'];
        foreach ($names as $name) {
            $path = 'member/NZPortfolio/' . $name . '.csv';
            if (Storage::exists($path)) {
                $csvs[$name] = Reader::createFromPath(storage_path('app/' . $path))->fetchAll();
            } else {
                $csvs[$name] = null;
            }
        }
        $chart = null;
        if (Storage::exists('member/NZPortfolio/chartOfMoment.csv')) {
            $chart = Reader::createFromPath(storage_path('app/member/NZPortfolio/chartOfMoment.csv'));
        }
        $text = Post::where('type', 'NZPortfolio')->where('title', 'NZPortfolio')->first();
        $trial = "false";
        if (me()->isTrial() && me()->payment_date > DateTime::createFromFormat('j-M-Y', '4-Feb-2016')) {
            $trial = "true";
        }

        return view('member.nzportfolio')->with('csvs', $csvs)->with('text', $text)
            ->with('trial', $trial)
            ->with('chart', $csvs['chartOfMoment'] ? $csvs['chartOfMoment'] : null)
            ->with('thematicExpsoure', $csvs['sectorBreakdown'] ? $csvs['sectorBreakdown'] : null)
            ->with('industryWeightings', $csvs['regionalBreakdown'] ? $csvs['regionalBreakdown'] : null);
    }


    public function Thematics()
    {
        $text = Post::where('type', 'Thematics')->where('title', 'Thematics')->first();
        return view('member.thematics')->with('text', $text);
    }

    public function Dividend()
    {
        $text = Post::where('type', 'Dividend')->where('title', 'Dividend')->first();
        $trial = "false";
        if (me()->isTrial() && me()->payment_date > DateTime::createFromFormat('j-M-Y', '4-Feb-2016')) {
            $trial = "true";
        }

        return view('member.dividend')->with('text', $text)->with('trial', $trial);
    }


    public function ConcentratedPortfolio()
    {
        $csvs = [];
        $names = ['auIndustryWeightings', 'nzIndustryWeightings', 'auThematicExposure', 'nzThematicExposure'];
        foreach ($names as $name) {
            $path = 'member/Concentrated/' . $name . '.csv';
            if (Storage::exists($path)) {
                $csvs[$name] = Reader::createFromPath(storage_path('app/' . $path))->fetchAll();
            } else {
                $csvs[$name] = null;
            }
        }
        $text = Post::where('type', 'Concentrated')->where('title', 'Concentrated')->first();
        $trial = "false";
        if (me()->isTrial() && me()->payment_date > DateTime::createFromFormat('j-M-Y', '3-Feb-2016')) {
            $trial = "true";
        }

        return view('member.concentrated-portfolio')->with('csvs', $csvs)->with('text', $text)
            ->with('trial', $trial);
    }


    /*public function getChartData()
    {
        if (Storage::exists('member/home/chartData.csv'))
            return Storage::get('member/home/chartData.csv');
        return null;
    }*/


}