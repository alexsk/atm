<?php namespace App\Http\Controllers\Member;

use App\Models\Document;
use App\Models\Report;
use App\Models\Note;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class ReportController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {

    }

    public function index()
    {
        $topTrades = Report::where('topTrades', 1)->take(3)->latest()->get();
        $monthly = Report::where('monthly', 1)->take(3)->latest()->get();
        $daily = Report::where('daily', 1)->take(3)->latest()->get();
        $investor = Report::where('investor', 1)->take(3)->latest()->get();
        $ytdNotes = Note::take(2)->where('pick', '1')->latest()->get();

        return view('member.report.index', compact('topTrades', 'monthly', 'daily', 'investor'))
            ->with('ytdNotes', ($ytdNotes->count() == 2) ? $ytdNotes : null);
    }

    public function getByType($type)
    {
        $reports = Report::where($type, 1)->latest()->paginate(25);
        $ytdNotes = Note::take(2)->where('pick', '1')->latest()->get();
        $report = null;
        if (count($reports) > 0) {
            $report = $reports[0];
        }
        $doc = Document::where('category', 'reports')->where('categoryId', $report->id)->orderBy('id', 'DESC')->first();
        return view('member.report.byType')->with('reports', $reports)
            ->with('type', $type)
            ->with('doc', $doc)
            ->with('ytdNotes', ($ytdNotes->count() == 2) ? $ytdNotes : null);
    }

    /*public function show($type, $id){
        $report = Report::where($type, 1)->where('id', $id)->first();
        $reports = Report::where($type, 1)->where('id', '<>',  $id)->take(3)->latest()->get();
        if(!$report){
            return redirect()->back();
        }
        $doc = Document::where('category', 'reports')->where('categoryId', $report->id)->orderBy('id', 'DESC')->first();

        return view('member.report.show')->with('report', $report)
            ->with('reports', $reports)
            ->with('type', $type)
            ->with('doc', $doc);
    }*/

    public function show($type, $id)
    {
        $report = Report::where($type, 1)->where('id', $id)->first();
        $reports = Report::where($type, 1)->where('id', '<>', $id)->take(3)->latest()->get();
        if (!$report) {
            return redirect()->back();
        }
        $doc = Document::where('category', 'reports')->where('categoryId', $report->id)->orderBy('id', 'DESC')->first();

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.box.com/2.0/files/" . $doc->newBoxId . "?fields=expiring_embed_link",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer WwdsQPRT6nDIiyA53SNsl5wMD6PXWKE2",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $result = json_decode($response, true);
            $doc->viewUrl = $result["expiring_embed_link"]["url"] ."?showDownload=true";
            $doc->save();
        }

        return view('member.report.show')->with('report', $report)
            ->with('reports', $reports)
            ->with('type', $type)
            ->with('doc', $doc);
    }

    public function test()
    {

        $doc = Document::where('path', 'member/home/forHomePage.pdf')->first();
        $headers = array("Authorization: Bearer WwdsQPRT6nDIiyA53SNsl5wMD6PXWKE2");
        $doc->path = "member/home/forHomePage.pdf";
        $path = $doc->path;
        $doc->name = basename($path);
        $json = json_encode(array(
            'name' => $doc->name,
            'parent' => array('id' => '0')
        ));

        if (file_exists(storage_path("app/" . $path))) {
            try {
                $params = array(
                    'attributes' => $json,
                    'file' => new \CurlFile(storage_path("app/" . $path), 'application/pdf', $doc->name)
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://upload.box.com/api/2.0/files/content");
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_ENCODING, "");
                curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
                curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

                $response = curl_exec($ch);
                curl_close($ch);

                $arr = json_decode($response, true);
                //dd($arr);
                $file = $arr['entries'][0];
                $id = $file['id'];
                $doc->newBoxId = $id;
                $doc->documentId = $id;
                $doc->status = 'uploaded';
                $doc->author = "ATM Team";
                $doc->type = 'pdf';
                $doc->save();

                $viewdoc = Document::where('path', 'member/home/forHomePage.pdf')->first();

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.box.com/2.0/files/". $viewdoc -> newBoxId. "?fields=expiring_embed_link",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "authorization: Bearer WwdsQPRT6nDIiyA53SNsl5wMD6PXWKE2",
                        "cache-control: no-cache",
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    $result = json_decode($response, true);
                    dd($result);
                    $viewdoc->viewUrl = $result["expiring_embed_link"]["url"];
                    $viewdoc->save();
                }

            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
    }


    public function massreupload()
    {
        $documents = Document::whereNull('newBoxId')->take(30)->get();
        $headers = array("Authorization: Bearer WwdsQPRT6nDIiyA53SNsl5wMD6PXWKE2");
        foreach ($documents as $doc) {

            $path = $doc->path;
            if (!isset($doc->name) || trim($doc->name) === '') {
                $doc->name = basename($path);
            }
            $json = json_encode(array(
                'name' => $doc->name,
                'parent' => array('id' => '0')
            ));

            if (file_exists(storage_path("app/" . $path))) {
                try {
                    $params = array(
                        'attributes' => $json,
                        'file' => new \CurlFile(storage_path("app/" . $path), 'application/pdf', $doc->name)
                    );
                    if (!isset($doc->newBoxId) || trim($doc->newBoxId) === '') {
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, "https://upload.box.com/api/2.0/files/content");
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_ENCODING, "");
                        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

                        $response = curl_exec($ch);
                        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        $errNo = curl_errno($ch);
                        $errStr = curl_error($ch);
                        curl_close($ch);

                        $arr = json_decode($response, true);
                        //dd($arr);
                        $file = $arr['entries'][0];
                        $id = $file['id'];
                        $doc->newBoxId = $id;
                        $doc->save();
                        echo($doc->newBoxId);
                        echo(PHP_EOL);
                    }
                } catch (\Exception $e) {
                    echo($e->getMessage());
                    echo(PHP_EOL);
                }
            }
        }
    }
}