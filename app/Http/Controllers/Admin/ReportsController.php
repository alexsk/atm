<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\RemoveDocument;
use App\Models\Report;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repos\ReportRepository;

class ReportsController extends Controller
{
    private $repo;
    /**
     * @param  \App\Repos\ReportRepository $repo
     */
    public function __construct(ReportRepository $repo)
    {
        $this->repo = $repo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(forRestmod($this->repo->all(['id', 'name', 'author', 'topTrades', 'monthly', 'daily', 'investor', 'created_at', 'updated_at']), 'reports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        if($this->repo->create($input)){
            return $this->respondOK('resource created');
        }else{
            return $this->errorNotFound('resource not created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(forRestmod($this->repo->find($id), 'report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        if ($input['featured'] > 0)//make  latest featured report a non-featured one
        {
            $featuredReport = Report::whereFeatured('1')->first();
            if ($featuredReport){
                $featuredReport->featured = 0;
                $featuredReport->save();
            }
        }
        if($this->repo->update($input, $id)){
            // this is crazy but we need to ensure featured report is updated
            $featuredReport = Report::whereId($input["id"])->first();
            if($input['featured']){
                $featuredReport->featured = 1;
            }
            else
                $featuredReport->featured = 0;
            $featuredReport -> save();
            return $this->respondOK("resource updated");
        }else{
            return $this->errorNotFound('resource not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);
        $this->dispatch(new RemoveDocument('reports', $id));
    }
}
