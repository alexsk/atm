<?php

namespace App\Http\Controllers\Admin;

use App\Models\Note;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repos\NoteRepository;
use Validator;

class NotesController extends Controller
{
    private $repo;

    /**
     * @param  \App\Repos\NoteRepository $repo
     */
    public function __construct(NoteRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(forRestmod($this->repo->all(['id', 'title', 'quote', 'created_at', 'updated_at']), 'notes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        if ($this->repo->create($input)) {
            return $this->respondOK();
        } else {
            return $this->errorNotFound('resource not created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(forRestmod($this->repo->find($id), 'note'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        if ($this->repo->update($input, $id)) {
            return $this->respondOK();
        } else {
            return $this->errorNotFound('resource not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->repo->delete($id)) {
            $path = public_path().'/uploads/notes/' . $id;
            if (\File::exists($path)) {
                \File::deleteDirectory($path);
            }
        }
    }

    public function smart_resize_image($file,
                                       $string             = null,
                                       $width              = 0,
                                       $height             = 0,
                                       $proportional       = false,
                                       $output             = 'file',
                                       $delete_original    = true,
                                       $use_linux_commands = false,
                                       $quality = 100
    ) {

        if ( $height <= 0 && $width <= 0 ) return false;
        if ( $file === null && $string === null ) return false;
        # Setting defaults and meta
        $info                         = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
        $image                        = '';
        $final_width                  = 0;
        $final_height                 = 0;
        list($width_old, $height_old) = $info;
        $cropHeight = $cropWidth = 0;
        # Calculating proportionality
        if ($proportional) {
            if      ($width  == 0)  $factor = $height/$height_old;
            elseif  ($height == 0)  $factor = $width/$width_old;
            else                    $factor = min( $width / $width_old, $height / $height_old );
            $final_width  = round( $width_old * $factor );
            $final_height = round( $height_old * $factor );
        }
        else {
            $final_width = ( $width <= 0 ) ? $width_old : $width;
            $final_height = ( $height <= 0 ) ? $height_old : $height;
            $widthX = $width_old / $width;
            $heightX = $height_old / $height;

            $x = min($widthX, $heightX);
            $cropWidth = ($width_old - $width * $x) / 2;
            $cropHeight = ($height_old - $height * $x) / 2;
        }
        # Loading image to memory according to type
        switch ( $info[2] ) {
            case IMAGETYPE_JPEG:  $file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);  break;
            case IMAGETYPE_GIF:   $file !== null ? $image = imagecreatefromgif($file)  : $image = imagecreatefromstring($string);  break;
            case IMAGETYPE_PNG:   $file !== null ? $image = imagecreatefrompng($file)  : $image = imagecreatefromstring($string);  break;
            default: return false;
        }


        # This is the resizing/resampling/transparency-preserving magic
        $image_resized = imagecreatetruecolor( $final_width, $final_height );
        if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
            $transparency = imagecolortransparent($image);
            $palletsize = imagecolorstotal($image);
            if ($transparency >= 0 && $transparency < $palletsize) {
                $transparent_color  = imagecolorsforindex($image, $transparency);
                $transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
                imagefill($image_resized, 0, 0, $transparency);
                imagecolortransparent($image_resized, $transparency);
            }
            elseif ($info[2] == IMAGETYPE_PNG) {
                imagealphablending($image_resized, false);
                $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
                imagefill($image_resized, 0, 0, $color);
                imagesavealpha($image_resized, true);
            }
        }
        imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);


        # Taking care of original, if needed
        if ( $delete_original ) {
            if ( $use_linux_commands ) exec('rm '.$file);
            else @unlink($file);
        }
        # Preparing a method of providing result
        switch ( strtolower($output) ) {
            case 'browser':
                $mime = image_type_to_mime_type($info[2]);
                header("Content-type: $mime");
                $output = NULL;
                break;
            case 'file':
                $output = $file;
                break;
            case 'return':
                return $image_resized;
                break;
            default:
                break;
        }

        # Writing image according to type to the output destination and image quality
        switch ( $info[2] ) {
            case IMAGETYPE_GIF:   imagegif($image_resized, $output);    break;
            case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output, $quality);   break;
            case IMAGETYPE_PNG:
                $quality = 9 - (int)((0.9*$quality)/10.0);
                imagepng($image_resized, $output, $quality);
                break;
            default: return false;
        }
        return true;
    }

    /**
     * Manipulate note's image
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function image($id)
    {
        $note = Note::where('id', $id)->first();
        if(!$note){
            return $this->errorNotFound('there is no such resource');
        }
        $path = public_path().'/uploads/notes/' . $id;
        if (\Request::isMethod('post')) {
            $input = \Request::all();
            $file = $input['file'];
            $rules = array('file' => 'required|image');
            $validator = Validator::make(array('file' => $file), $rules);
            if ($validator->passes()) {
                $name = 'image.' . $file->guessExtension();
                $name260 ="image-260x260-resize." . $file->guessExtension();
                $name200 ="image-200x200-resize.". $file->guessExtension();
                $name475 ="image-475x475-resize.". $file->guessExtension();
                if(!\File::exists($path)){
                    \File::makeDirectory($path, 0777, true);
                }else{
                    \File::deleteDirectory($path);
                    if(!\File::exists($path)){
                        \File::makeDirectory($path, 0777, true);
                    }
                }
                if($file->move($path, $name)) {
                    $file = $path . '/' . $name;
                    $resizedFile = $path . '/' . $name260;
                    $this->smart_resize_image($file , null, 260, 260, false , $resizedFile , false , false ,100 );

                    $resizedFile = $path . '/' . $name200;
                    $this->smart_resize_image($file , null, 200, 200, false , $resizedFile , false , false ,100 );

                    $resizedFile = $path . '/' . $name475;
                    $this->smart_resize_image($file , null, 475, 475, false , $resizedFile , false , false ,100 );
                }
                $note->image = '/uploads/notes/' . $id . '/' . $name;
                $note->update();
                return $this->respondOK('file uploaded');
            }
            return $this->errorInternalError('file not uploaded, failed to pass validation');
        } elseif (\Request::isMethod('get')) {
            if($note->image){
                if (\File::exists(public_path().$note->image)) {
                    $type = pathinfo($note->image, PATHINFO_EXTENSION);
                    $data = \File::get(public_path().$note->image);
                    $output = [];
                    $output['image'] = 'image';
                    $output['data'] = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    return json_encode($output);
                }
            }else{
                return json_encode(['image' => 'image', 'data' => '']);
            }
        } elseif (\Request::isMethod('delete')) {
            if (\File::exists($path)) {
                \File::deleteFile($path);
            }
            $note->image = null;
            $note->update();
        }
    }
}
