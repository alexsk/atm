<?php namespace App\Http\Controllers\Admin;

use App\Repos\UserRepository;

class DashboardController extends Controller
{

    /**
     * The Dashboard instance.
     *
     * @var \App\Repos\UserRepository
     */
    protected $userRepo;
    /**
     * Create a new AdminController instance.
     *
     * @param  \App\Repos\UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json($this->userRepo->paginate(5));
    }
}