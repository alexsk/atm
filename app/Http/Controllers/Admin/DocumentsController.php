<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Document;
use App\Jobs\SendFileToBoxViewer;
use Validator;
use Storage;

class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param string
     * @param integer
     * @return \Illuminate\Http\Response
     */
    public function index($category, $id)
    {
        return Document::where('category', $category)->where('categoryId', $id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $categoryId = $input['categoryId'];
        $category = $input['category'];
        $unique = isset($input['unique']) ? true : false;
        $file = $input['file'];
        $fileName = $file->getClientOriginalName();
        $name = $input['name'];
        $path = 'member/' . $category . '/' . $categoryId . '/' . $fileName;
        $rules = array('file' => 'required|mimes:pdf');
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {
            Storage::put(
                $path,
                file_get_contents($file->getRealPath())
            );

            if (file_exists(storage_path("app/" . $path))) {
                try {

                    $doc = null;
                    if ($unique) {
                        $doc = Document::where('category', $category)->where('categoryId', $categoryId)->first();
                    } else {
                        $doc = Document::where('path', $path)->first();
                    }
                    if (!$doc) {
                        $doc = new Document();
                    }

                    $headers = array("Authorization: Bearer WwdsQPRT6nDIiyA53SNsl5wMD6PXWKE2");
                    $doc->path = $path;
                    $doc->name = basename($path);
                    $json = json_encode(array(
                        'name' => $doc->name,
                        'parent' => array('id' => '0')
                    ));

                    $params = array(
                        'attributes' => $json,
                        'file' => new \CurlFile(storage_path("app/" . $path), 'application/pdf', $doc->name)
                    );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://upload.box.com/api/2.0/files/content");
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_ENCODING, "");
                    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $arr = json_decode($response, true);
                    $file = $arr['entries'][0];
                    $id = $file['id'];
                    $doc->newBoxId = $id;
                    $doc->documentId = $id;
                    $doc->status = 'uploaded';
                    $doc->author = "ATM Team";
                    $doc->type = 'pdf';
                    $doc->categoryId = $categoryId;
                    $doc->category = $category;
                    $doc->name = $name;
                    $doc->save();
                } catch (\Exception $e) {
                    return $this->errorInternalError('error uploading');
                }
            }
            return $this->respondOK('file uploaded');
        }
        return $this->errorInternalError('file not uploaded, failed to pass validation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(forRestmod($this->repo->find($id), 'stock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        if($this->repo->update($input, $id)){
            return $this->respondOK("resource updated");
        }else{
            return $this->errorNotFound('resource not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doc = Document::where('id', $id)->first();
        $doc->delete();
    }

}
