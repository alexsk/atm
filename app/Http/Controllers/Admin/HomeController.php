<?php namespace App\Http\Controllers\Admin;

use App\Jobs\SendFileToBoxViewer;
use App\Models\Document;
use App\Models\Stock;
use App\Models\Post;
use Carbon\Carbon;
use Sunra\PhpSimple\HtmlDomParser;
use League\Csv\Writer;
use League\Csv\Reader;
use Request;
use Validator;
use Storage;
use DateTime;

class HomeController extends Controller
{
    protected $nzPortfolioHtml;
    protected $auPortfolioHtml;
    protected $auConcentrated;
    protected $nzConcentrated;

    /**
     * Display a top trades table.
     *
     * @return Response
     */
    public function topTradesTable()
    {
        $csv = null;
        if (Storage::exists('member/home/topTrades.csv')) {
            $csv = Reader::createFromPath(storage_path('app/member/home/topTrades.csv'));
        } else {
            $csv = Reader::createFromString('NZDUSD,0.6358,+1.3%', "\n");
        }
        return response()->json($csv);
    }


    /**
     * Same top trades table
     * @return Response
     */
    public function saveTopTradesTable()
    {
        $input = Request::all();
        $csv = Writer::createFromFileObject(new \SplTempFileObject());
        foreach ($input as $row) {
            $csv->insertOne($row);
        }
        Storage::put('member/home/topTrades.csv', $csv);
    }

    /**
     * Upload pdf and csv files for member home page.
     *
     * @return Response
     */
    public function upload()
    {
        $file = Request::file('file');
        $rules = array('file' => 'required|mimes:csv,txt,pdf');
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {
            if ($file->getMimeType() == 'application/pdf') {
                Storage::put(
                    'member/home/forHomePage.pdf',
                    file_get_contents($file->getRealPath())
                );

                $path = 'member/home/forHomePage.pdf';

                if (file_exists(storage_path("app/" . $path))) {
                    try {
                        $doc = Document::where('path', 'member/home/forHomePage.pdf')->first();
                        $headers = array("Authorization: Bearer WwdsQPRT6nDIiyA53SNsl5wMD6PXWKE2");
                        $doc->name = basename($path);
                        $json = json_encode(array(
                            'name' => $doc->name,
                            'parent' => array('id' => '0')
                        ));

                        $params = array(
                            'attributes' => $json,
                            'file' => new \CurlFile(storage_path("app/" . $path), 'application/pdf', $doc->name)
                        );

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, "https://upload.box.com/api/2.0/files/content");
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_ENCODING, "");
                        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

                        $response = curl_exec($ch);
                        curl_close($ch);

                        $arr = json_decode($response, true);
                        //dd($arr);
                        $file = $arr['entries'][0];
                        $id = $file['id'];
                        $doc->newBoxId = $id;
                        $doc->documentId = $id;
                        $doc->status = 'uploaded';
                        $doc->author = "ATM Team";
                        $doc->type = 'pdf';
                        $doc->save();
                    } catch (\Exception $e) {
                        return $this->errorInternalError('error uploading');
                    }
                }
            } else {
                Storage::put(
                    'member/home/chartData.csv',
                    file_get_contents($file->getRealPath())
                );
            }
            return $this->respondOK('file uploaded');
        }
        return $this->errorInternalError('file not uploaded, failed to pass validation');
    }


    /**
     * Upload  csv files for stocks.
     *
     * @return Response
     */
    public function uploadStocks()
    {
        $file = Request::file('file');
        $rules = array('file' => 'required|mimes:csv,txt');
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {
            if (!ini_get("auto_detect_line_endings")) {
                ini_set("auto_detect_line_endings", '1');
            }
            Storage::put(
                'member/home/tempData.csv',
                file_get_contents($file->getRealPath())
            );
            $inputCsv = Reader::createFromPath(storage_path('app/member/home/tempData.csv'));
            $inputCsv->setDelimiter(',');
            $this->nzPortfolioHtml = HtmlDomParser::str_get_html($this->getCentralHtml('NZPortfolio'));
            $this->auPortfolioHtml = HtmlDomParser::str_get_html($this->getCentralHtml('AUPortfolio'));
            $post = Post::where('type', 'Concentrated')->where('title', 'Concentrated')->first();
            $this->auConcentrated = HtmlDomParser::str_get_html($post->topLeft);
            $this->nzConcentrated = HtmlDomParser::str_get_html($post->topRight);
            foreach ($inputCsv as $key => $row) {
                if ($key == 0) {
                    continue;
                }
                $code = $row[0];
                $stock = Stock::firstOrCreate(['code' => $code]);
                $stock->name = $row[1];
                $stock->recommendation = $row[2];
                $stock->recommendation_date = $row[3] ? Carbon::createFromFormat('d/m/Y', $row[3]) : Carbon::now();
                $stock->sharePrice = $row[4];
                $stock->price = $row[5];
                $stock->dividend = $row[6];
                $stock->marketCap = $row[7];
                $stock->capitalReturn = $row[8];
                $stock->totalReturn = $row[9];
                $stock->updated_at = Carbon::now();
                $stock->risk = $row[11];
                $stock->save();
                $this->updateHtmls($row);
            }
            $this->saveCentralHtml();
            $post->topLeft = $this->auConcentrated->save();
            $post->topRight = $this->nzConcentrated->save();
            $post->save();
            return $this->respondOK('file uploaded');
        }
        return $this->errorInternalError('file not uploaded, failed to pass validation');
    }

    private function updateHtmls($row = [])
    {
        foreach ($this->auPortfolioHtml->find('tbody td') as $td) {
            if (strcasecmp($td->innertext, $row[0]) == 0) { //ticker
                $td->next_sibling()->next_sibling()->innertext = "$row[12]"; //portfolio_share
                try {
                    $recommendation_date = DateTime::createFromFormat('n/j/Y', $row[3])->format('j M y');;
                    $td->next_sibling()->next_sibling()->next_sibling()->innertext = "$row[2] - $recommendation_date"; //recommendation
                } catch (Exception $e) {
                    echo("<script>console.log('PHP Exception: $e');</script>");
                }
                $td->next_sibling()->next_sibling()->next_sibling()->next_sibling()->innertext = "$row[9]"; //return since added
            }
        }

        foreach ($this->nzPortfolioHtml->find('tbody td') as $td) {
            $arr[] = $td->innertext;
            if (strcasecmp($td->innertext, $row[0]) == 0) {
                $td->next_sibling()->next_sibling()->innertext = "$row[13]";
                $recommendation_date = DateTime::createFromFormat('n/j/Y', $row[3])->format('j M y');;
                $td->next_sibling()->next_sibling()->next_sibling()->innertext = "$row[2] - $recommendation_date"; //recommendation
                $td->next_sibling()->next_sibling()->next_sibling()->next_sibling()->innertext = "$row[9]"; //return since added
            }
        }

        foreach ($this->auConcentrated->find('table td') as $td) {
            if (strcasecmp($td->innertext, $row[0]) == 0) {
                $td->next_sibling()->next_sibling()->innertext = "$row[14]";
                $recommendation_date = DateTime::createFromFormat('n/j/Y', $row[3])->format('j M y');;
                $td->next_sibling()->next_sibling()->next_sibling()->innertext = "$row[2] - $recommendation_date"; //recommendation
            }
        }

        foreach ($this->nzConcentrated->find('table td') as $td) {
            if (strcasecmp($td->innertext, $row[0]) == 0) {
                $td->next_sibling()->next_sibling()->innertext = "$row[15]";
                $recommendation_date = DateTime::createFromFormat('n/j/Y', $row[3])->format('j M y');;
                $td->next_sibling()->next_sibling()->next_sibling()->innertext = "$row[2] - $recommendation_date"; //recommendation
            }
        }
    }

    private function getCentralHtml($type = null)
    {
        $post = Post::where('type', $type)->where('title', $type)->first();
        if ($post) {
            return $post->body;
        }
        $path = 'member/' . $type . '/centralText.html';
        if (Storage::exists($path)) {
            return Storage::get($path);
        }
        return null;
    }

    private function saveCentralHtml()
    {
        $this->auPortfolioHtml->save(storage_path('app/member/AUPortfolio/centralText.html'));
        $this->nzPortfolioHtml->save(storage_path('app/member/NZPortfolio/centralText.html'));
        $types = ['AUPortfolio', 'NZPortfolio'];
        foreach ($types as $key => $type) {
            $post = Post::where('type', $type)->where('title', $type)->first();
            if (!$post) {
                $post = new Post();
            }
            $body = '';
            $path = 'member/' . $type . '/centralText.html';
            if (Storage::exists($path)) {
                $body = Storage::get($path);
            }
            $post->type = $type;
            $post->title = $type;
            $post->body = $body;
            $post->save();
        }
    }

}