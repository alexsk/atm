<?php

namespace App\Repos;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use App\Repos\StockRepository;
use App\Models\Stock;

/**
 * Class PostRepositoryEloquent
 * @package namespace App\Repos;
 */
class StockRepositoryEloquent extends BaseRepository implements StockRepository, CacheableInterface
{
    use CacheableRepository;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Stock::class;
    }

    /**
     * Specify Validator Rules
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'code' => 'required|unique:stocks',
            'name' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'code' => 'required|unique:stocks',
            'name' => 'required',
        ]
    ];

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Specify User Presenter
     * @return string
     */
    public function presenter()
    {
        return "App\\Presenters\\StockPresenter";
    }
}
