<?php

namespace App\Repos;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PostRepository
 * @package namespace App\Repos;
 */
interface ReportRepository extends RepositoryInterface
{
    //
}
