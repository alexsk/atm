<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;
class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('App\Repos\UserRepository', 'App\Repos\UserRepositoryEloquent');
        App::bind('App\Repos\StockRepository', 'App\Repos\StockRepositoryEloquent');
        App::bind('App\Repos\NoteRepository', 'App\Repos\NoteRepositoryEloquent');
        App::bind('App\Repos\ReportRepository', 'App\Repos\ReportRepositoryEloquent');
    }
}
