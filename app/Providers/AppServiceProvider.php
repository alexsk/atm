<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Stock;
use App\Models\Note;
use App\Models\Report;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $focus = Stock::take(1)->where('returnSince','=','featured')->latest()->first();
        if (!$focus){
            $focus = Stock::take(1)->where('code','=','AMZN.NASDAQ')->latest()->first();
        }

        $featured = Report::take(1)->where('featured','>','0')->latest()->first();
        if (!$featured) {
            $featured = Report::take(1)->where('investor', '>', '0')->latest()->first();
        }

        $newsletter = Note::take(1)->where('newsletter','=','1')->latest()->get();


        view()->share('focus', $focus ? $focus : null);
        view()->share('featured', $featured ? $featured : null);
        view()->share('newsletter', $newsletter ? $newsletter : null);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
