<?php

namespace App\Jobs;

use Illuminate\Contracts\Bus\SelfHandling;
use Storage;
use App\Models\Document;

class RemoveDocument extends Job implements SelfHandling
{
    private $category;
    private $categoryId;

    /**
     * Create a new job instance.
     * @param string
     * @param int
     */
    public function __construct($category = '', $categoryId = 0)
    {
        $this->category = $category;
        $this->categoryId = $categoryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $documents = Document::where('category', $this->category)->where('categoryId', $this->categoryId)->get();
        if(!$documents){
            return;
        }
        foreach($documents as $document) {
            if (Storage::exists($document->path)) {
                Storage::delete($document->path);
            }
            $document->delete();
        }
    }
}
